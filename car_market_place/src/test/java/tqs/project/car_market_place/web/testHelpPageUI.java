package tqs.project.car_market_place.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import java.util.concurrent.TimeUnit;

public class testHelpPageUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testHelpPage(){
        //verificar se a página Help foi bem formada

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("help")).click(); //mudar para a página help

        List<WebElement> titles = driver.findElements(By.cssSelector(".dropdown"));
        int count_titles  = titles.size();

        //como definimos as FAQ hardcoded o valor será sempre 4
        assertThat(count_titles==4,is(true));
    }
}
