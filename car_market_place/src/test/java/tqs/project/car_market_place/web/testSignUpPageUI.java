package tqs.project.car_market_place.web;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;



public class testSignUpPageUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }
    

    @Test
    public void testSignUpPage() throws InterruptedException {
        /*
            Simula o registo de um utilizador com campos não utilizados, verificado que não é emitida uma mensagem de erro
         */

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("a-not-logged")).click();

        //verifica se todos os elementos necessários ao registo estão presentes
        assertThat(driver.findElements( By.id("inserted_name")).size()==1,is(true));
        assertThat(driver.findElements( By.id("contact")).size()==1,is(true));
        assertThat(driver.findElements( By.id("inserted_user")).size()==1,is(true));
        assertThat(driver.findElements( By.id("inserted_email")).size()==1,is(true));
        assertThat(driver.findElements( By.id("password")).size()==1,is(true));
        assertThat(driver.findElements( By.id("confirm_password")).size()==1,is(true));

        //introduzir nome e contacto do utilizador
        driver.findElement(By.id("inserted_name")).sendKeys("José Carlos");
        driver.findElement(By.id("contact")).sendKeys("911234546");

        //Username nunca utilizado - a mensagem exibida deve ser que este se encontra disponível
        driver.findElement(By.id("inserted_user")).sendKeys("selenium_test");
        WebElement msg_user = driver.findElement(By.id("user_msg"));
        String username_error_msg = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", msg_user);
        assertThat(username_error_msg.equals("Username available!"),is(true));

        //Email nunca utilizado - a mensagem exibida deve ser que este se encontra disponível
        //É verificado também se o email inserido tem um formato de email, i.e. , possuí uma arroba @
        driver.findElement(By.id("inserted_email")).sendKeys("selenium_test@ua.pt");
        WebElement msg_email = driver.findElement(By.id("email"));
        String email_error_msg = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", msg_email);
        System.out.println(email_error_msg);
        assertThat(email_error_msg.equals("Valid email."),is(true));

        //Password e Confirmação - verifica se são iguais , sendo que a mensagem exibida deve indicar esse facto
        driver.findElement(By.id("password")).sendKeys("pass1");
        driver.findElement(By.id("confirm_password")).sendKeys("pass1");
        WebElement msg_password = driver.findElement(By.id("message"));
        String pw_error_msg = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", msg_password);
        assertThat(pw_error_msg.equals("Passwords do match."),is(true));
    }
}
