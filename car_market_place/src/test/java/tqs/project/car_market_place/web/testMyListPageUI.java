package tqs.project.car_market_place.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import java.util.concurrent.TimeUnit;

public class testMyListPageUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testMyListPageWithoutLogin(){
        //verifica se, quando o utilizador tenta aceder à página que lista todos os anuncios do utilizador,
        //é exibida uma mensagem que indica que este tem de fazer log in previamente

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("mylist")).click();//mudar para a página sell

        //Navbar alert existe
        assertThat(driver.findElements( By.id("must-login")).size()!=0,is(true));
        assertThat(driver.findElements( By.id("must-login")).size()==1,is(true));
    }

    @Test
    public void testMyListPageWithLogin(){
        ////verifica se, quando o utilizador tenta aceder à página para adicionar um anúncio, não é exibida uma mensagem
        //que indica que este tem de fazer log in previamente

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();

        driver.findElement(By.id("mylist")).click(); //mudar para a pagina sell

        //Navbar alert não existe
        assertThat(driver.findElements( By.id("must-login")).size()==0,is(true));

        //verificar se o form está presente
        //count%3 == 0 porque para cada anuncio são geradas 3 divs : uma para o anuncio (que contem as outras duas)
        //uma para a imagem e outra para a parte textual
        //no caso do utilizador não ter anuncios count=0
        List<WebElement> forms = driver.findElements(By.cssSelector(".container div"));
        int count = forms.size();
        assertThat(count%3==0,is(true));

        //verificar se os modals foram gerados
        //cada anuncio tem um modal (para editar o anuncio) (#anuncios = count/3)
        //logo o contador de anuncios/3 tem de ser igual ao numero de modals
        int count_modals = driver.findElements(By.xpath("//div[@id='modals']/div")).size();
        assertThat((count/3)==count_modals,is(true));
    }
}
