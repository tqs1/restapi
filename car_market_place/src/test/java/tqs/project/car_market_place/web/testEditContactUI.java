package tqs.project.car_market_place.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class testEditContactUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testEditContactWithWrongCurrentContact(){
        //Teste a edicão do contacto com o contacto actual errado

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editContact();");
        }

        driver.findElement(By.id("InputContact")).sendKeys("9199999"); //WRONG CONTACT
        driver.findElement(By.id("InputNewContact")).sendKeys("912222222");
        driver.findElement(By.id("sub")).click();

        WebElement element = driver.findElement(By.id("actual_contact"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents.equals("Wrong contact"),is(true));
    }


    @Test
    public void testEditContactWithEqualContacts(){
        //Teste a edicão do contacto com o novo contacto e o actual iguais

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editContact();");
        }

        driver.findElement(By.id("InputContact")).sendKeys("917894561"); //ACTUAL CONTACT CORRECT
        driver.findElement(By.id("InputNewContact")).sendKeys("917894561");
        driver.findElement(By.id("sub")).click();

        WebElement element = driver.findElement(By.id("span_new_contact"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents.equals("Contacts are the same."),is(true));
    }

    @Test
    public void testEditContactWithBothEmptyFields()  {
        //Teste a edicão do contacto com ambos os campos vazios

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editContact();");
        }

        driver.findElement(By.id("InputContact")).sendKeys("");
        driver.findElement(By.id("InputNewContact")).sendKeys("");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputContact")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));



    }


    @Test
    public void testEditContactWithEmptyCurrentContact()  {
        //Teste a edicão do contacto com o contacto actual vazio

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editContact();");
        }

        driver.findElement(By.id("InputContact")).sendKeys("");
        driver.findElement(By.id("InputNewContact")).sendKeys("91212121");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputContact")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));
    }


    @Test
    public void testEditContactWithEmptyNewContact()  {
        //Teste a edicão do username com ambos os campos vazios

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editContact();");
        }

        driver.findElement(By.id("InputContact")).sendKeys("91111111111111");
        driver.findElement(By.id("InputNewContact")).sendKeys("");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputNewContact")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));
    }

}
