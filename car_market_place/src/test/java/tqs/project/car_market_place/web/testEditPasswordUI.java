package tqs.project.car_market_place.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class testEditPasswordUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testEditPasswordWithWrongCurrentPassword(){
        //Teste a edicão da password com a password actual errada

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editPassword();");
        }

        driver.findElement(By.id("InputPassword")).sendKeys("wrong_password");
        driver.findElement(By.id("InputNewPassword")).sendKeys("new_password");
        driver.findElement(By.id("InputNewPasswordConfirm")).sendKeys("new_password");
        driver.findElement(By.id("sub")).click();

        WebElement element = driver.findElement(By.id("actual_password"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents.equals("Wrong password"),is(true));
    }

    @Test
    public void testEditPasswordWithDifferentNewPasswords(){
        //Testa a edicão da password com passwords novas diferentes (nova e respectiva confirmação)

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editPassword();");
        }

        driver.findElement(By.id("InputPassword")).sendKeys("jose");
        driver.findElement(By.id("InputNewPassword")).sendKeys("new_password");
        driver.findElement(By.id("InputNewPasswordConfirm")).sendKeys("new_password_dif");
        driver.findElement(By.id("sub")).click();


        WebElement element = driver.findElement(By.id("span_new_password"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents.equals("New Passwords are different"),is(true));
    }

    @Test
    public void testEditPasswordWithBothEmptyFields()  {
        //Teste a edicão da passwrod com ambos os campos vazios

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editPassword();");
        }

        driver.findElement(By.id("InputPassword")).sendKeys("");
        driver.findElement(By.id("InputNewPassword")).sendKeys("");
        driver.findElement(By.id("InputNewPasswordConfirm")).sendKeys("");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputPassword")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));
    }


    @Test
    public void testEditPasswordWithEmptyCurrentPassword()  {
        //Teste a edicão da password com password actual vazia

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editPassword();");
        }

        driver.findElement(By.id("InputPassword")).sendKeys("");
        driver.findElement(By.id("InputNewPassword")).sendKeys("jose1");
        driver.findElement(By.id("InputNewPasswordConfirm")).sendKeys("jose1");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputPassword")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));

    }

    @Test
    public void testEditPasswordWithEmptyNewPassword()  {
        //Teste a edicão da password com nova password vazia

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editPassword();");
        }

        driver.findElement(By.id("InputPassword")).sendKeys("jose");
        driver.findElement(By.id("InputNewPassword")).sendKeys("");
        driver.findElement(By.id("InputNewPasswordConfirm")).sendKeys("");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputNewPassword")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));

    }

}
