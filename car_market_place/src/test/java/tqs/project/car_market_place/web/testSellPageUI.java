package tqs.project.car_market_place.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import java.util.concurrent.TimeUnit;

public class testSellPageUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testSellPageWithoutLogin(){
        //verifica se, quando o utilizador tenta aceder à página para adicionar um anúncio, é exibida uma mensagem
        //que indica que este tem de fazer log in previamente

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("sell")).click();//mudar para a página sell

        //Navbar alert existe
        assertThat(driver.findElements( By.id("must-login")).size()!=0,is(true));
        assertThat(driver.findElements( By.id("must-login")).size()==1,is(true));
    }

    @Test
    public void testSellPageWithLogin()  {
        //verifica se, quando o utilizador tenta aceder à página para adicionar um anúncio, não é exibida uma mensagem
        //que indica que este tem de fazer log in previamente

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose");
        driver.findElement(By.id("psw")).sendKeys("jose");
        driver.findElement(By.id("btLogin")).click();

        driver.findElement(By.id("sell")).click(); //mudar para a pagina sell

        //Navbar alert não existe
        assertThat(driver.findElements( By.id("must-login")).size()==0,is(true));

        //verificar se o form está presente
        assertThat(driver.findElements(By.id("myForm")).size()==1,is(true));

        //verificar se estão todos os campos do form
        assertThat(driver.findElements(By.className("form-group")).size()==13,is(true));
    }
}
