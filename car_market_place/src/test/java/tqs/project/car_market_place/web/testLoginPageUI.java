package tqs.project.car_market_place.web;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class testLoginPageUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testLoginPageWrongCredentials(){
        /*
            Tentar iniciar sessão com credenciais erradas
            É exibido um alert
        */

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click();

        assertThat(driver.findElements( By.id("uname")).size()==1,is(true));
        assertThat(driver.findElements( By.id("psw")).size()==1,is(true));

        //Wrong credentials
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("wrongpassword"); //wrong password
        driver.findElement(By.id("btLogin")).click();

        //verificar se é exibido o alert
        boolean alertExists = isAlertPresent();
        assertThat(alertExists,is(true));
    }

    @Test
    public void testLoginPageWithCorrectCredentials(){
        /*
            Iniciar sessão com as credenciais correctas
            username: jose
            password: jose
         */

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click();

        //verificar a presença dos campos necessários
        assertThat(driver.findElements( By.id("uname")).size()==1,is(true));
        assertThat(driver.findElements( By.id("psw")).size()==1,is(true));

        //verificar que não há informação de que o utilizador está logged in (==0)
        assertThat(driver.findElements( By.id("li-logged-as")).size()==0,is(true));

        //Inserir as credenciais correctas
        driver.findElement(By.id("uname")).sendKeys("jose");
        driver.findElement(By.id("psw")).sendKeys("jose");
        driver.findElement(By.id("btLogin")).click();

        //Login tem sucesso logo não existe alerta de que as credenciais estão erradas.
        //O user é redireccionado para index.html
        boolean alertExists = isAlertPresent();
        assertThat(alertExists,is(false));

        //na página index.html deverá ter a indicação de que está logged in ("Logged as: jose")
        assertThat(driver.findElements( By.id("li-logged-as")).size()==1,is(true));
        assertThat(driver.findElement(By.id("li-logged-as")).getText().equals("Logged as: jose |"),is(true));

        //não deve ver o botão para fazer login
        assertThat(driver.findElements(By.id("login-a-not-logged")).size()==0,is(true));

        //botão logout
        assertThat(driver.findElements( By.id("a-logged")).size()==1,is(true));
    }

    @Test
    public void testLoginPage(){
        //verificar apenas se a página está bem formada

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click();

        //verificar se os elementos necessários estão presentes
        assertThat(driver.findElements( By.id("uname")).size()==1,is(true));
        assertThat(driver.findElements( By.id("psw")).size()==1,is(true));
    }

    //método auxiliar para verificar se é exibido um alerta
    public boolean isAlertPresent()
    {
        try {
            driver.switchTo().alert();
            return true;
        }
        catch (NoAlertPresentException Ex) {
            return false;
        }
    }
}
