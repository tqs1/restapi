package tqs.project.car_market_place.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class testCarsPageUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testButtonsCarsPage() {
        //verifica se os botões de pesquisa estão disponíveis

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        assertThat(driver.findElements( By.id("btBrand") ).size()!=0,is(true));
        assertThat(driver.findElements( By.id("btModel") ).size()!=0,is(true));
        assertThat(driver.findElements( By.id("btSearch") ).size()!=0,is(true));
    }

    @Test
    public void testSearchResultsCarsPage() {
        //verifica se os anuncios são exibidos quando a página é carregada
        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));

        //Elemento search_results existe
        assertThat(driver.findElements( By.id("search_results") ).size()!=0,is(true));

        //Elemento search_results está a carregar os anúncios (tem conteúdo)
        WebElement element = driver.findElement(By.id("search_results"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents!="",is(true));

    }

    @Test
    public void testModalCreatedCarsPage(){
        //verifica se o modal criado para cada anúncio tem conteúdo, i.e., é diferente de ""

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));

        //Verificar se os modals foram criados
        WebElement modal = driver.findElement(By.id("adModal"));
        String modal_content = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", modal);
        assertThat(modal_content!="",is(true));
    }

    @Test
    public void testSearchForSpecificBrandAndModelCar(){
        /*
            Testa a pesquisa por uma marca e modelo específicos para carros
            Neste caso: Honda Civic
         */
        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));

        driver.findElement(By.id("btBrand")).click();
        driver.findElement(By.id("1")).click(); //Honda

        driver.findElement(By.id("btModel")).click();
        driver.findElement(By.id("Civic")).click(); //Civic

        driver.findElement(By.id("btSearch")).click();

        //Clicar para abrir o modal
        driver.findElement(By.id("a-modal")).click();

        //Verificar se os anuncios exibidos correspondem à marca e modelo procurados
        WebElement adbrand = driver.findElement(By.id("adModalBrand"));
        String adbrandcontent = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", adbrand);
        assertThat(adbrandcontent.equals("Honda"),is(true)); //marca procurada

        WebElement admodel = driver.findElement(By.id("adModalModel"));
        String admodelcontent = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerText;", admodel);
        assertThat(admodelcontent.equals("Civic"),is(true)); //modelo procurado
    }
}
