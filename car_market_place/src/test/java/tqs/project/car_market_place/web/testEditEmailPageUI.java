package tqs.project.car_market_place.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class testEditEmailPageUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testEditEmailWithBothEmptyFields()  {
        //Teste a edicão do email com ambos os campos vazios

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editEmail();");
        }

        driver.findElement(By.id("InputEmail")).sendKeys("");
        driver.findElement(By.id("InputNewEmail")).sendKeys("");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputEmail")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));
    }

    @Test
    public void testEditEmailWithWrongCurrentEmail(){
        //Teste a edicão do email com o email actual errado

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editEmail();");
        }

        driver.findElement(By.id("InputEmail")).sendKeys("wrong_email@ua.pt");
        driver.findElement(By.id("InputNewEmail")).sendKeys("new_email@ua.pt");
        driver.findElement(By.id("sub")).click();

        WebElement element = driver.findElement(By.id("actual_email"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents.equals("Wrong email"),is(true));
    }

    @Test
    public void testEditEmailWithEqualEmails(){
        //Teste a edicão do email com email actual e novo iguais

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editEmail();");
        }

        driver.findElement(By.id("InputEmail")).sendKeys("jose@ua.pt");
        driver.findElement(By.id("InputNewEmail")).sendKeys("jose@ua.pt");
        driver.findElement(By.id("sub")).click();

        WebElement element = driver.findElement(By.id("span_new_email"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents.equals("Emails are the same."),is(true));
    }

    @Test
    public void testEditEmailWithEmptyCurrentEmail()  {
        //Teste a edicão do email com o email actual vazio

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editEmail();");
        }

        driver.findElement(By.id("InputEmail")).sendKeys("");
        driver.findElement(By.id("InputNewEmail")).sendKeys("new_email@ua.pt");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputEmail")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));

    }

    @Test
    public void testEditEmailWithEmptyNewEmail()  {
        //Teste a edicão do email com novo email vazio

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("editEmail();");
        }

        driver.findElement(By.id("InputEmail")).sendKeys("current_email@ua.pt");
        driver.findElement(By.id("InputNewEmail")).sendKeys("");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputNewEmail")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));

    }

}
