package tqs.project.car_market_place.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class testEditUsernameUI {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testEditUsernameWithBothEmptyFields()  {
        //Teste a edicão do username com ambos os campos vazios

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        //introduzir actual e username
        driver.findElement(By.id("InputUsername")).sendKeys("");
        driver.findElement(By.id("InputNewUsername")).sendKeys("");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputUsername")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));

    }

    @Test
    public void testEditUsernameWithWrongCurrentUsername(){
        //Teste a edicão do username com o primeiro campo (username actual) errado

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        //introduzir actual e novo username
        driver.findElement(By.id("InputUsername")).sendKeys("wrong_username");
        driver.findElement(By.id("InputNewUsername")).sendKeys("new_username");
        driver.findElement(By.id("sub")).click();

        WebElement element = driver.findElement(By.id("actual_username"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents.equals("Wrong current username."),is(true));
    }

    @Test
    public void testEditUsernameWithEqualUsernames(){
        //Teste a edicão do username com actual e novo utilizador iguais

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina edit

        //introduzir actual e novo username
        driver.findElement(By.id("InputUsername")).sendKeys("jose");
        driver.findElement(By.id("InputNewUsername")).sendKeys("jose");
        driver.findElement(By.id("sub")).click();

        WebElement element = driver.findElement(By.id("span_new_username"));
        String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", element);
        assertThat(contents.equals("New username must be differente from current."),is(true));
    }


    @Test
    public void testEditUsernameWithEmptyCurrentUsername()  {
        //Teste a edicão do username com username actual vazio

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        driver.findElement(By.id("InputUsername")).sendKeys("");
        driver.findElement(By.id("InputNewUsername")).sendKeys("new_username");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputUsername")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));

    }

    @Test
    public void testEditUsernameWithEmptyNewUsername()  {
        //Teste a edicão do username com novo username vazio

        //driver.get("file:///home/joao/webapplication/index.html");
        driver.get("http://127.0.0.1/");
        driver.manage().window().setSize(new Dimension(1920, 985));
        driver.findElement(By.id("login-a-not-logged")).click(); //mudar para a página login
        driver.findElement(By.id("uname")).sendKeys("jose"); //user
        driver.findElement(By.id("psw")).sendKeys("jose");  //password
        driver.findElement(By.id("btLogin")).click();
        driver.findElement(By.id("edit")).click(); //mudar para a pagina sell

        driver.findElement(By.id("InputUsername")).sendKeys("jose");
        driver.findElement(By.id("InputNewUsername")).sendKeys("");
        driver.findElement(By.id("sub")).click();

        String message = driver.findElement(By.id("InputNewUsername")).getAttribute("validationMessage");
        assertThat(message.equals("Please fill out this field."),is(true));

    }
}
