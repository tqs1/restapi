package tqs.project.car_market_place.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import tqs.project.car_market_place.entities.Users;
import tqs.project.car_market_place.exceptions.InvalidInput;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UsersRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UsersRepository usersRepository;

    Users user1;
    Users user2;

    @BeforeEach
    void setUp(){
        user1 = new Users("carinaneves", "carina@ua.pt", "Carina", "123Carina", null, "919191919");
        user2 = new Users("magalhaes", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");

        testEntityManager.persistAndFlush(user1);
        testEntityManager.persistAndFlush(user2);
    }

    @Test
    void getUsers(){
        List<String> allUsernames = usersRepository.getUsers();
        assertThat(allUsernames.size()).isEqualTo(2);
        assertThat(allUsernames.get(0)).isEqualTo(user1.getUsername());
        assertThat(allUsernames.get(1)).isEqualTo(user2.getUsername());
    }

    @Test
    void getEmails(){
        List<String> allEmails = usersRepository.getEmails();
        assertThat(allEmails.size()).isEqualTo(2);
        assertThat(allEmails.get(0)).isEqualTo(user1.getEmail());
        assertThat(allEmails.get(1)).isEqualTo(user2.getEmail());
    }

    @Test
    void getPassword(){
        String password = usersRepository.getPassword(user1.getUsername());
        assertThat(password).isEqualTo(user1.getPassword());
    }

    @Test
    void getEmail(){
        String email = usersRepository.getEmail(user2.getUsername());
        assertThat(email).isEqualTo(user2.getEmail());
    }

    @Test
    void getContact(){
        String contact = usersRepository.getContact(user2.getUsername());
        assertThat(contact).isEqualTo(user2.getContact());
    }

    @Test
    void findByUsername(){
        Users users3 = usersRepository.findByUsername(user1.getUsername());
        assertThat(users3.getUsername()).isEqualTo(user1.getUsername());
        assertThat(users3.getName()).isEqualTo(user1.getName());
        assertThat(users3.getEmail()).isEqualTo(user1.getEmail());
    }

    @Test
    void findIdByUsername(){
        long userID = usersRepository.findIdByUsername(user1.getUsername());
        assertThat(userID).isEqualTo(user1.getUserID());
    }
}