package tqs.project.car_market_place.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import tqs.project.car_market_place.entities.Brand;
import tqs.project.car_market_place.entities.Model;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class BrandRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private BrandRepository brandRepository;

    private Brand b1;
    private Brand b2;
    private Brand b3;
    private Brand b4;

    private Model m1;
    private Model m2;

    @BeforeEach
    void setUp() {
        b1 = new Brand("Mercedes", "Car");
        b2 = new Brand("BMW", "Car");
        b3 = new Brand("Honda", "Moto");
        b4 = new Brand("Yamaha", "Moto");
        m1 = new Model("Classe A", b1);
        m2 = new Model("Classe B", b1);

        testEntityManager.persistAndFlush(b1);
        testEntityManager.persistAndFlush(b2);
        testEntityManager.persistAndFlush(b3);
        testEntityManager.persistAndFlush(b4);
        testEntityManager.persistAndFlush(m1);
        testEntityManager.persistAndFlush(m2);
    }

    @Test
    void getCarBrands(){
        List<Brand> allCars = brandRepository.getCarBrands();
        assertThat(allCars.size()).isEqualTo(2);
        assertThat(allCars.get(0).getBrand()).isEqualTo(b1.getBrand());
        assertThat(allCars.get(1).getBrand()).isEqualTo(b2.getBrand());
        assertThat(allCars.get(0).getType()).isEqualTo(b1.getType());
        assertThat(allCars.get(1).getType()).isEqualTo(b2.getType());
    }

    @Test
    void getMotoBrands(){
        List<Brand> allMotos = brandRepository.getMotoBrands();
        assertThat(allMotos.size()).isEqualTo(2);
        assertThat(allMotos.get(0).getBrand()).isEqualTo(b3.getBrand());
        assertThat(allMotos.get(1).getBrand()).isEqualTo(b4.getBrand());
        assertThat(allMotos.get(0).getType()).isEqualTo(b3.getType());
        assertThat(allMotos.get(1).getType()).isEqualTo(b4.getType());
    }

    @Test
    void getModelsByBrandID(){
        List<String> allModels = brandRepository.getModelsByBrandID(b1.getBrandID());
        assertThat(allModels.size()).isEqualTo(2);
        assertThat(allModels.get(0)).isEqualTo(m1.getModel());
        assertThat(allModels.get(1)).isEqualTo(m2.getModel());
    }
}