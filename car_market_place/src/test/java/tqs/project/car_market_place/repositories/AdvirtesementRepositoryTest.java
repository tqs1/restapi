package tqs.project.car_market_place.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import tqs.project.car_market_place.dto.NewAdvirtesementDTO;
import tqs.project.car_market_place.entities.Advirtesement;
import tqs.project.car_market_place.entities.Users;

import java.sql.Timestamp;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class AdvirtesementRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private AdvirtesementRepository advirtesementRepository;

    private NewAdvirtesementDTO adDTO1;
    private NewAdvirtesementDTO adDTO2;
    private NewAdvirtesementDTO adDTO3;
    private Advirtesement ad1;
    private Advirtesement ad2;
    private Advirtesement ad3;
    private Users user1;

    @BeforeEach
    void setUp() {
        Timestamp time = new Timestamp(2020 - 05 - 19);
        adDTO1 = new NewAdvirtesementDTO("magalhaes", "Car", true, time, 06, 2014, "Honda", "Civic", null, "Diesel", 95874, 1500, 120, "Black", 15500, null, null, "Aveiro");
        adDTO2 = new NewAdvirtesementDTO("magalhaes", "Car", true, time, 06, 2014, "Seat", "Ibiza", null, "Diesel", 195874, 1500, 120, "White", 12500, null, null, "Braga");
        adDTO3 = new NewAdvirtesementDTO("magalhaes", "Moto", true, time, 06, 2014, "Honda", "CBR", null, "Petrol", 10000, 1000, 70, "Black", 9500, null, null, "Aveiro");
        user1 = new Users("magalhaes", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");
        ad1 = new Advirtesement(adDTO1, user1);
        ad2 = new Advirtesement(adDTO2, user1);
        ad3 = new Advirtesement(adDTO3, user1);

        testEntityManager.persistAndFlush(user1);
        testEntityManager.persistAndFlush(ad1);
        testEntityManager.persistAndFlush(ad2);
        testEntityManager.persistAndFlush(ad3);
    }

    @Test
    void getAllCars(){
        List<Advirtesement> allAdsCars = advirtesementRepository.getAll("Car");
        assertThat(allAdsCars.get(0).getType()).isEqualTo(ad1.getType());
        assertThat(allAdsCars.get(0).getBrand()).isEqualTo(ad1.getBrand());
        assertThat(allAdsCars.get(1).getType()).isEqualTo(ad2.getType());
        assertThat(allAdsCars.get(1).getBrand()).isEqualTo(ad2.getBrand());
    }

    @Test
    void getAllMotos(){
        List<Advirtesement> allAdsMotos = advirtesementRepository.getAll("Moto");
        assertThat(allAdsMotos.get(0).getType()).isEqualTo(ad3.getType());
        assertThat(allAdsMotos.get(0).getBrand()).isEqualTo(ad3.getBrand());
    }

    @Test
    void findByID(){
        Advirtesement ad = advirtesementRepository.findByID(ad1.getAdID());
        assertThat(ad.getUserID()).isEqualTo(ad1.getUserID());
        assertThat(ad.getType()).isEqualTo(ad1.getType());
        assertThat(ad.getBrand()).isEqualTo(ad1.getBrand());
        assertThat(ad.getModel()).isEqualTo(ad1.getModel());
        assertThat(ad.getPrice()).isEqualTo(ad1.getPrice());
        assertThat(ad.getLocation()).isEqualTo(ad1.getLocation());
    }

    @Test
    void delete(){
        int deleteAd = advirtesementRepository.delete(ad1.getAdID());
        assertThat(deleteAd).isEqualTo(1);
    }

    @Test
    void getUserSells(){
        List<Advirtesement> userSells = advirtesementRepository.getUserSells(user1.getUserID());
        assertThat(userSells.get(0).getUserID()).isEqualTo(ad1.getUserID());
        assertThat(userSells.get(0).getBrand()).isEqualTo(ad1.getBrand());
        assertThat(userSells.get(0).getModel()).isEqualTo(ad1.getModel());
        assertThat(userSells.get(0).getPrice()).isEqualTo(ad1.getPrice());
        assertThat(userSells.get(0).getLocation()).isEqualTo(ad1.getLocation());
    }

    @Test
    void getFilterAd(){
        List<Advirtesement> filterAd = advirtesementRepository.getFilterAd("Car", "Honda", "", "Diesel", 0);
        assertThat(filterAd.get(0).getUserID()).isEqualTo(ad1.getUserID());
        assertThat(filterAd.get(0).getBrand()).isEqualTo(ad1.getBrand());
        assertThat(filterAd.get(0).getModel()).isEqualTo(ad1.getModel());
        assertThat(filterAd.get(0).getPrice()).isEqualTo(ad1.getPrice());
        assertThat(filterAd.get(0).getLocation()).isEqualTo(ad1.getLocation());
    }
}