package tqs.project.car_market_place;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ToJSON {
    public static String ToJson(Object object){
        try{
            return new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(object);
        }catch (JsonProcessingException e){
            throw new RuntimeException();
        }
    }
}
