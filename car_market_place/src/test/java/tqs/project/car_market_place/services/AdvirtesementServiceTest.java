package tqs.project.car_market_place.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.HttpServerErrorException;
import tqs.project.car_market_place.dto.*;
import tqs.project.car_market_place.entities.Advirtesement;
import tqs.project.car_market_place.entities.Users;
import tqs.project.car_market_place.exceptions.InvalidBodyParameters;
import tqs.project.car_market_place.exceptions.InvalidInput;
import tqs.project.car_market_place.repositories.AdvirtesementRepository;
import tqs.project.car_market_place.repositories.UsersRepository;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AdvirtesementServiceTest {

    @Mock(lenient = true)
    private AdvirtesementRepository advirtesementRepository;

    @Mock(lenient = true)
    private UsersRepository usersRepository;

    @InjectMocks
    private AdvirtesementService advirtesementService;

    private NewAdvirtesementDTO adDTO1;
    private NewAdvirtesementDTO adDTO2;
    private NewAdvirtesementDTO adDTO3;

    private Advirtesement ad1;
    private Advirtesement ad2;
    private Advirtesement ad3;

    private Users user1;

    private String type;
    private String brand;
    private String model = "";
    private String fuel;

    private float price = 0;
    private long invalidID = 1343124;


    @BeforeEach
    void setUp() {
        Timestamp time = new Timestamp(2020-05-19);
        adDTO1= new NewAdvirtesementDTO("magalhaes", "Car", true, time, 06, 2014, "Honda", "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");
        adDTO2= new NewAdvirtesementDTO("magalhaes", "Car", true, time, 06, 2014, "Seat", "Ibiza",null, "Diesel", 195874, 1500,  120, "White", 12500, null, null, "Braga");
        adDTO3= new NewAdvirtesementDTO("magalhaes", "Moto", true, time, 06, 2014, "Honda", "CBR",null, "Petrol", 10000, 1000,  70, "Black", 9500, null, null, "Aveiro");

        user1 = new Users("magalhaes", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");

        ad1 = new Advirtesement(adDTO1, user1);
        ad2 = new Advirtesement(adDTO2, user1);
        ad3 = new Advirtesement(adDTO3, user1);

        List<Advirtesement> allCars = Arrays.asList(ad1, ad2);
        List<Advirtesement> allMotos = Arrays.asList(ad3);
        List<Advirtesement> allSells = Arrays.asList(ad1);
        List<Advirtesement> filterCar = Arrays.asList(ad1);

        type = ad1.getType();
        brand = ad1.getBrand();
        fuel = ad1.getFuel();

        Mockito.when(advirtesementRepository.getAll("Car")).thenReturn(allCars);
        Mockito.when(advirtesementRepository.getAll("Moto")).thenReturn(allMotos);

        Mockito.when(advirtesementRepository.findByID(ad1.getAdID())).thenReturn(ad1);
        Mockito.when(advirtesementRepository.findByID(invalidID)).thenThrow(HttpServerErrorException.InternalServerError.class);

        Mockito.when(advirtesementRepository.save(ad1)).thenReturn(ad1);
        Mockito.when(advirtesementRepository.save(ad2)).thenReturn(ad2);
        Mockito.when(advirtesementRepository.save(ad3)).thenReturn(ad3);

        Mockito.when(usersRepository.findByUsername(adDTO1.getUsername())).thenReturn(user1);
        Mockito.when(usersRepository.findByUsername(adDTO2.getUsername())).thenReturn(user1);
        Mockito.when(usersRepository.findByUsername(adDTO3.getUsername())).thenReturn(user1);

        Mockito.when(advirtesementRepository.delete(ad2.getAdID())).thenReturn(1);
        Mockito.when(advirtesementRepository.delete((long) 787)).thenThrow(NullPointerException.class);

        Mockito.when(advirtesementRepository.getUserSells(user1.getUserID())).thenReturn(allSells);
        Mockito.when(advirtesementRepository.getFilterAd(type,brand,model,fuel, price)).thenReturn(filterCar);
    }

    @Test
    void getAllCars(){
        List<AdvirtesementSearchDTO> cars = advirtesementService.getAll("Car");
        assertThat(cars.get(0).getUserName()).isEqualTo(user1.getName());
        assertThat(cars.get(0).getType()).isEqualTo(ad1.getType());
        assertThat(cars.get(0).getBrand()).isEqualTo(ad1.getBrand());
        assertThat(cars.get(0).getModel()).isEqualTo(ad1.getModel());
        assertThat(cars.get(0).getPrice()).isEqualTo(ad1.getPrice());
        assertThat(cars.get(0).getLocation()).isEqualTo(ad1.getLocation());
        assertThat(cars.get(1).getUserName()).isEqualTo(user1.getName());
        assertThat(cars.get(1).getType()).isEqualTo(ad2.getType());
        assertThat(cars.get(1).getBrand()).isEqualTo(ad2.getBrand());
        assertThat(cars.get(1).getModel()).isEqualTo(ad2.getModel());
        assertThat(cars.get(1).getPrice()).isEqualTo(ad2.getPrice());
        assertThat(cars.get(1).getLocation()).isEqualTo(ad2.getLocation());
    }

    @Test
    void getAllMotos(){
        List<AdvirtesementSearchDTO> motos = advirtesementService.getAll("Moto");
        assertThat(motos.get(0).getUserName()).isEqualTo(user1.getName());
        assertThat(motos.get(0).getType()).isEqualTo(ad3.getType());
        assertThat(motos.get(0).getBrand()).isEqualTo(ad3.getBrand());
        assertThat(motos.get(0).getModel()).isEqualTo(ad3.getModel());
        assertThat(motos.get(0).getPrice()).isEqualTo(ad3.getPrice());
        assertThat(motos.get(0).getLocation()).isEqualTo(ad3.getLocation());
    }


    @Test
    void getAdByID_whenIDIsValid(){
        AdvirtesementDTO adByID = advirtesementService.getAdByID(ad1.getAdID());
        assertThat(adByID.getType()).isEqualTo(ad1.getType());
        assertThat(adByID.getBrand()).isEqualTo(ad1.getBrand());
        assertThat(adByID.getModel()).isEqualTo(ad1.getModel());
        assertThat(adByID.getPrice()).isEqualTo(ad1.getPrice());
        assertThat(adByID.getLocation()).isEqualTo(ad1.getLocation());
    }

    @Test
    void getAdByID_whenIDisInvalid(){
        assertThrows(InvalidBodyParameters.class, ()->advirtesementService.getAdByID(invalidID));
    }

    @Test
    void changeAd_whenChangesAreInvalid(){
        UpdatedAdvirtesementDTO updatedAD = new UpdatedAdvirtesementDTO(06, 2014, "Honda", "Civic",null, "Diesel", 95874, 1500,  120,"Black", 15500, null, null, "Aveiro");
        assertThrows(InvalidBodyParameters.class, ()->advirtesementService.changeAd(invalidID, updatedAD));
    }

    @Test
    void setSold_withValidID(){
        boolean sold = advirtesementService.setSold( ad1.getAdID());
        assertThat(sold).isEqualTo(true);
    }

    @Test
    void setSold_withInvalidID(){
        boolean sold = advirtesementService.setSold((long)1231312);
        assertThat(sold).isEqualTo(false);
    }

    @Test
    void deleteAd_withValidID(){
        int deleted = advirtesementService.deleteAd(ad2.getAdID());
        assertThat(deleted).isEqualTo(1);
    }

    @Test
    void deleteAd_withInvalidID(){
        assertThrows(InvalidInput.class, ()->advirtesementService.deleteAd((long) 787));
    }

    @Test
    void getUserSells(){
        List<AdvirtesementSearchDTO> userSells = advirtesementService.getUserSells(user1.getUsername());
        assertThat(userSells.get(0).getUserName()).isEqualTo(user1.getName());
        assertThat(userSells.get(0).getBrand()).isEqualTo(ad1.getBrand());
        assertThat(userSells.get(0).getModel()).isEqualTo(ad1.getModel());
        assertThat(userSells.get(0).getPrice()).isEqualTo(ad1.getPrice());
        assertThat(userSells.get(0).getLocation()).isEqualTo(ad1.getLocation());
    }

    @Test
    void getFilterAd_withValidFields(){
        List<AdvirtesementSearchDTO> filterAd = advirtesementService.getFilterAd(type, brand, model, fuel, price);
        assertThat(filterAd.get(0).getUserName()).isEqualTo(user1.getName());
        assertThat(filterAd.get(0).getBrand()).isEqualTo(ad1.getBrand());
        assertThat(filterAd.get(0).getModel()).isEqualTo(ad1.getModel());
        assertThat(filterAd.get(0).getPrice()).isEqualTo(ad1.getPrice());
        assertThat(filterAd.get(0).getLocation()).isEqualTo(ad1.getLocation());
    }
}

