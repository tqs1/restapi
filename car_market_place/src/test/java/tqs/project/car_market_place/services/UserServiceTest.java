package tqs.project.car_market_place.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import tqs.project.car_market_place.dto.UsersDTO;
import tqs.project.car_market_place.entities.Users;
import tqs.project.car_market_place.exceptions.InvalidUser;
import tqs.project.car_market_place.repositories.UsersRepository;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock(lenient = true)
    private UsersRepository usersRepository;

    @InjectMocks
    private UserService userService;

    private Users user1;
    private Users user2;

    @BeforeEach
    void setUp() {

        user1 = new Users("carinaneves", "carina@ua.pt", "Carina", "123Carina", null, "919191919");
        user2 = new Users("magalhaes", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");

        List<String> allUsernames = Arrays.asList(user1.getUsername(), user2.getUsername());
        List<String> allEmails = Arrays.asList(user1.getEmail(), user2.getEmail());

        Mockito.when(usersRepository.getUsers()).thenReturn(allUsernames);
        Mockito.when(usersRepository.getEmails()).thenReturn(allEmails);
        Mockito.when(usersRepository.getPassword(user1.getUsername())).thenReturn(user1.getPassword());
        Mockito.when(usersRepository.save(user2)).thenReturn(user2);
        Mockito.when(usersRepository.getEmail(user1.getUsername())).thenReturn(user1.getEmail());
        Mockito.when(usersRepository.getContact(user1.getUsername())).thenReturn(user1.getContact());
        Mockito.when(usersRepository.findByUsername(user2.getUsername())).thenReturn(user2);
    }

    @Test
    void getUsers(){
        List<String> users = userService.getUsers();
        assertThat(users)
                .hasSize(2)
                .extracting(String::toString)
                .contains(user1.getUsername(), user2.getUsername());
    }

    @Test
    void getEmails(){
        List<String> emails = userService.getEmails();
        assertThat(emails)
                .hasSize(2)
                .extracting(String::toString)
                .contains(user1.getEmail(), user2.getEmail());
    }

    @Test
    void getPassword_withValidUsername(){
        String password = userService.getPassword(user1.getUsername());
        assertThat(password)
                .isEqualTo(user1.getPassword());
    }

    @Test
    void getPassword_withInvalidUsername(){
        assertThrows(InvalidUser.class, ()->userService.getPassword("blabla"));
    }

    @Test
    void addNewUser_withValidDTO(){
        UsersDTO usersDTO = new UsersDTO("magalhaes", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");
        Users newUser = userService.addNewUser(usersDTO);
        assertThat(newUser.getUsername()).isEqualTo(usersDTO.getUsername());
        assertThat(newUser.getEmail()).isEqualTo(usersDTO.getEmail());
        assertThat(newUser.getName()).isEqualTo(usersDTO.getName());
        assertThat(newUser.getPassword()).isEqualTo(usersDTO.getPassword());
    }

    @Test
    void getEmail_withValidUsername(){
        String email = userService.getEmail(user1.getUsername());
        assertThat(email)
                .isEqualTo(user1.getEmail());
    }

    @Test
    void getEmail_withInvalidUsername(){
        assertThrows(InvalidUser.class, ()->userService.getEmail("blabla"));
    }

    @Test
    void getContact_withValidUsername(){
        String contact = userService.getContact(user1.getUsername());
        assertThat(contact)
                .isEqualTo(user1.getContact());
    }

    @Test
    void getContact_withInvalidUsername(){
        assertThrows(InvalidUser.class, ()->userService.getContact("blabla"));
    }

    @Test
    void changeUsername_withValidUsername(){
        Users user3 = new Users("magalhaes97", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");
        user3.setUsername("magalhaes");
        assertEquals("magalhaes", user3.getUsername());
        assertEquals("magalhaes@ua.pt", user3.getEmail());
        assertEquals("Magalhães", user3.getName());
    }

    @Test
    void changeUsername_withInvalidUsername(){
        String newUsername = "username";
        assertThrows(InvalidUser.class,
                ()->userService.changeUsername("blabla", newUsername));
    }

    @Test
    void changePasword_withValidUsername(){
        Users user3 = new Users("magalhaes97", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");
        user3.setPassword("magalhaes789");
        assertEquals("magalhaes97", user3.getUsername());
        assertEquals("magalhaes@ua.pt", user3.getEmail());
        assertEquals("Magalhães", user3.getName());
        assertEquals("magalhaes789", user3.getPassword());

    }

    @Test
    void changePassword_withInvalidUsername(){
        String newPassword = "password";
        assertThrows(InvalidUser.class,
                ()->userService.changeUsername("blabla", newPassword));
    }

    @Test
    void changeEmail_withValidUsername(){
        Users user3 = new Users("magalhaes97", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");
        user3.setEmail("magalhaes789@ua.pt");
        assertEquals("magalhaes97", user3.getUsername());
        assertEquals("magalhaes789@ua.pt", user3.getEmail());
        assertEquals("Magalhães", user3.getName());
        assertEquals("Magalhaes456", user3.getPassword());
    }

    @Test
    void changeEmail_withInvalidUsername(){
        String newEmail = "email@ua.pt";
        assertThrows(InvalidUser.class,
                ()->userService.changeUsername("blabla", newEmail));
    }

    @Test
    void changePhoto_withValidUsername(){
        Users user3 = new Users("magalhaes97", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");
        user3.setPhoto("photo");
        assertEquals("magalhaes97", user3.getUsername());
        assertEquals("magalhaes@ua.pt", user3.getEmail());
        assertEquals("Magalhães", user3.getName());
        assertEquals("Magalhaes456", user3.getPassword());
        assertEquals("photo", user3.getPhoto());
    }

    @Test
    void changePhoto_withInvalidUsername(){
        String newPhoto = "photo";
        assertThrows(InvalidUser.class,
                ()->userService.changeUsername("blabla", newPhoto));
    }

    @Test
    void changeContact_withValidUsername(){
        Users user3 = new Users("magalhaes97", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");
        user3.setContact("919191919");
        assertEquals("magalhaes97", user3.getUsername());
        assertEquals("magalhaes@ua.pt", user3.getEmail());
        assertEquals("Magalhães", user3.getName());
        assertEquals("Magalhaes456", user3.getPassword());
        assertEquals(null, user3.getPhoto());
        assertEquals("919191919", user3.getContact());
    }

    @Test
    void changeContact_withInvalidUsername(){
        String newContact = "919191987";
        assertThrows(InvalidUser.class,
                ()->userService.changeUsername("blabla", newContact));
    }
}