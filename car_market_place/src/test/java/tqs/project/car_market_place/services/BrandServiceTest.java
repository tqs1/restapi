package tqs.project.car_market_place.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.web.client.HttpClientErrorException;
import tqs.project.car_market_place.dto.BrandDTO;
import tqs.project.car_market_place.entities.Brand;
import tqs.project.car_market_place.entities.Model;
import tqs.project.car_market_place.exceptions.InvalidBrandID;
import tqs.project.car_market_place.repositories.BrandRepository;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BrandServiceTest {

    @Mock(lenient = true)
    private BrandRepository brandRepository;

    @InjectMocks
    private BrandService brandService;

    private Brand b1;
    private Brand b2;
    private Brand b3;
    private Brand b4;

    private Model m1;
    private Model m2;

    private long invalidID = 1343124;

    @BeforeEach
    void setUp() {
        b1 = new Brand("Mercedes", "Car");
        b2 = new Brand("BMW", "Car");
        b3 = new Brand("Honda", "Moto");
        b4 = new Brand("Yamaha", "Moto");
        m1 = new Model("Classe A", b1);
        m2 = new Model("Classe B", b1);

        List<Brand> allCars = Arrays.asList(b1,b2);
        List<Brand> allMotos = Arrays.asList(b3, b4);
        List<String> allModels = Arrays.asList(m1.getModel(), m2.getModel());

        Mockito.when(brandRepository.getCarBrands()).thenReturn(allCars);
        Mockito.when(brandRepository.getMotoBrands()).thenReturn(allMotos);
        Mockito.when(brandRepository.getModelsByBrandID(b1.getBrandID())).thenReturn(allModels);
        Mockito.when(brandRepository.getModelsByBrandID(invalidID)).thenThrow(HttpClientErrorException.NotFound.class);

    }

    @Test
    void getCarBrand(){
        List<BrandDTO> allCars = brandService.getCarBrands();
        assertThat(allCars.get(0).getBrand()).isEqualTo(b1.getBrand());
        assertThat(allCars.get(1).getBrand()).isEqualTo(b2.getBrand());
    }

    @Test
    void getMotoBrand(){
        List<BrandDTO> allMotos = brandService.getMotoBrands();
        assertThat(allMotos.get(0).getBrand()).isEqualTo(b3.getBrand());
        assertThat(allMotos.get(1).getBrand()).isEqualTo(b4.getBrand());
    }

    @Test
    void getModelsByBrandID(){
        List<String> allModels = brandService.getModelsByBrandID(b1.getBrandID());
        assertThat(allModels)
                .hasSize(2)
                .extracting(String::toString)
                .contains(m1.getModel(), m2.getModel());
    }

    @Test
    void getModelsByBrandID_withInvalidID(){
        assertThrows(InvalidBrandID.class, ()->brandService.getModelsByBrandID(invalidID));
    }

}