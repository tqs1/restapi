package tqs.project.car_market_place;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CarMarketPlaceApplicationTests {

    @Test
    void contextLoads() {
        assertThat(true).isTrue();
    }

}
