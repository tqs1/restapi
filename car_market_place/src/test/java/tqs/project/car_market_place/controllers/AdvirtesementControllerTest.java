package tqs.project.car_market_place.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import tqs.project.car_market_place.dto.*;
import tqs.project.car_market_place.entities.Advirtesement;
import tqs.project.car_market_place.exceptions.InvalidBodyParameters;
import tqs.project.car_market_place.exceptions.InvalidInput;
import tqs.project.car_market_place.services.AdvirtesementService;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static tqs.project.car_market_place.ToJSON.ToJson;

@WebMvcTest(AdvirtesementController.class)
class AdvirtesementControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AdvirtesementService advirtesementService;

    @Test
    void whenGetAdByID_thenReturnAdvirtesementDTO() throws Exception {
        Timestamp time = new Timestamp(2020-05-19);
        AdvirtesementDTO adDTO= new AdvirtesementDTO(1,"carolina", "123456789","carolina@ua.pt", "Car", true, time, 01, 2019,"Honda", "Civic", null, "Diesel",  18000, 1600, 120, "Black", 19500, null, "great condition", "Braga");

        given(advirtesementService.getAdByID(any())).willReturn(adDTO);
        mockMvc.perform(get("/carmarketplace/ad=1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.userName", is(adDTO.getUserName())))
                .andExpect(jsonPath("$.contacto", is(adDTO.getContacto())))
                .andExpect(jsonPath("$.email", is(adDTO.getEmail())))
                .andExpect(jsonPath("$.type", is(adDTO.getType())))
                .andExpect(jsonPath("$.state", is(adDTO.isState())))
                .andExpect(jsonPath("$.month", is(adDTO.getMonth())))
                .andExpect(jsonPath("$.year", is(adDTO.getYear())))
                .andExpect(jsonPath("$.brand", is(adDTO.getBrand())))
                .andExpect(jsonPath("$.model", is(adDTO.getModel())))
                .andExpect(jsonPath("$.version", is(adDTO.getVersion())))
                .andExpect(jsonPath("$.fuel", is(adDTO.getFuel())))
                .andExpect(jsonPath("$.kms", is(adDTO.getKms())))
                .andExpect(jsonPath("$.cylinder", is(adDTO.getCylinder())))
                .andExpect(jsonPath("$.power", is(adDTO.getPower())))
                .andExpect(jsonPath("$.color", is(adDTO.getColor())))
                .andExpect(jsonPath("$.image", is(adDTO.getImage())))
                .andExpect(jsonPath("$.annotation", is(adDTO.getAnnotation())))
                .andExpect(jsonPath("$.location", is(adDTO.getLocation())))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetAdByID_AndInvalidBodyParameters_thenReturnInternalServerError() throws Exception {
        given(advirtesementService.getAdByID(any())).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(get("/carmarketplace/ad=12223")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGetAll_thenReturnListAdvirtesementSearchDTO() throws Exception {
        AdvirtesementSearchDTO adDTO1= new AdvirtesementSearchDTO(1, 1, "carolina", "Car", true, "Honda", "Civic", null, 19500, null, "Braga");
        AdvirtesementSearchDTO adDTO2= new AdvirtesementSearchDTO(2, 2, "jose", "Car", true, "Seat", "Ibiza", null, 17500, null, "Aveiro");

        List<AdvirtesementSearchDTO> adCar= new ArrayList<>(Arrays.asList(adDTO1, adDTO2));

        given(advirtesementService.getAll(any())).willReturn(adCar);
        mockMvc.perform(get("/carmarketplace/all/type=Car")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].anuncioID", is(1)))
                .andExpect(jsonPath("$[0].userName", is(adDTO1.getUserName())))
                .andExpect(jsonPath("$[0].type", is(adDTO1.getType())))
                .andExpect(jsonPath("$[0].brand", is(adDTO1.getBrand())))
                .andExpect(jsonPath("$[0].location", is(adDTO1.getLocation())))
                .andExpect(jsonPath("$[1].anuncioID", is(2)))
                .andExpect(jsonPath("$[1].userName", is(adDTO2.getUserName())))
                .andExpect(jsonPath("$[1].type", is(adDTO2.getType())))
                .andExpect(jsonPath("$[1].brand", is(adDTO2.getBrand())))
                .andExpect(jsonPath("$[1].location", is(adDTO2.getLocation())));
    }

    @Test
    void whenGetAll_UsernameNotFound_thenReturnEmptyArray() throws Exception{
        String type  = "blabla";
        List toReturn = new ArrayList<>();

        given(advirtesementService.getAll(type)).willReturn(toReturn);
        mockMvc.perform(get("/carmarketplace/all/type="+ type)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(toReturn.toString()))
                .andExpect(status().isOk());
    }

    @Test
    void whenAddNewAd_thenReturnTrue() throws Exception {
        Timestamp time = new Timestamp(2020-05-19);
        Advirtesement ad = new Advirtesement("Car", true, time, 06, 2014, "Honda", "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");
        NewAdvirtesementDTO newAd= new NewAdvirtesementDTO("carolina","Car", true, time, 06, 2014, "Honda", "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");

        given(advirtesementService.addNewAd(any(NewAdvirtesementDTO.class))).willReturn(ad);
        mockMvc.perform(post("/carmarketplace/newAd")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(newAd)))
                .andExpect(jsonPath("$.type", is(ad.getType())))
                .andExpect(jsonPath("$.year", is(ad.getYear())))
                .andExpect(jsonPath("$.brand", is(ad.getBrand())))
                .andExpect(jsonPath("$.fuel", is(ad.getFuel())))
                .andExpect(jsonPath("$.color", is(ad.getColor())))
                .andExpect(jsonPath("$.location", is(ad.getLocation())))
                .andExpect(status().isOk());
    }

    @Test
    void whenAddNewAd_AndInvalidBodyParameters_thenReturnInternalServerError() throws Exception {
        Timestamp time = new Timestamp(2020-05-19);
        Advirtesement ad = new Advirtesement(null, true, time, 06, 2014, "Honda", "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");


        given(advirtesementService.addNewAd(any(NewAdvirtesementDTO.class))).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(post("/carmarketplace/newAd")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(ad)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenAddNewAd_AndContentInvalid_thenReturnBadRequest() throws Exception {
        Timestamp time = new Timestamp(2020-05-19);
        Advirtesement ad = new Advirtesement("Boat", true, time, 06, 2014, "Honda", "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");

        given(advirtesementService.addNewAd(any(NewAdvirtesementDTO.class))).willThrow(InvalidInput.class);
        mockMvc.perform(post("/carmarketplace/newAd")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(ad)))
                .andExpect(status().isBadRequest());
    }


    @Test
    void whenChangeAd_thenReturnUserDTO() throws Exception {
        UpdatedAdvirtesementDTO adUpdate = new UpdatedAdvirtesementDTO(06, 2014, "Honda", "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");
        Timestamp time = new Timestamp(2020-05-27);
        long adID = 1;
        AdvirtesementDTO adToReturn = new AdvirtesementDTO(1,"carolina", "123456789", "carolina@ua.pt", "Car", true, time, 06, 2014, "Honda", "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");

        given(advirtesementService.changeAd(anyLong(), any(UpdatedAdvirtesementDTO.class))).willReturn(adToReturn);
        mockMvc.perform(put("/carmarketplace/edit/ad=" + adID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(adUpdate)))
                .andExpect(jsonPath("$.userName", is(adToReturn.getUserName())))
                .andExpect(jsonPath("$.year", is(adUpdate.getYear())))
                .andExpect(jsonPath("$.brand", is(adUpdate.getBrand())))
                .andExpect(jsonPath("$.model", is(adUpdate.getModel())))
                .andExpect(jsonPath("$.version", is(adUpdate.getVersion())))
                .andExpect(jsonPath("$.fuel", is(adUpdate.getFuel())))
                .andExpect(jsonPath("$.kms", is(adUpdate.getKms())))
                .andExpect(jsonPath("$.cylinder", is(adUpdate.getCylinder())))
                .andExpect(jsonPath("$.power", is(adUpdate.getPower())))
                .andExpect(jsonPath("$.color", is(adUpdate.getColor())))
                .andExpect(jsonPath("$.image", is(adUpdate.getImage())))
                .andExpect(jsonPath("$.annotation", is(adUpdate.getAnnotation())))
                .andExpect(jsonPath("$.location", is(adUpdate.getLocation())))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeAd_AndInvalidBodyParameters_thenReturnInternalServerError() throws Exception {
        UpdatedAdvirtesementDTO adUpdate = new UpdatedAdvirtesementDTO(6, 2014, null, "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");
        long adID = 12312312;

        given(advirtesementService.changeAd(anyLong(), any(UpdatedAdvirtesementDTO.class))).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(put("/carmarketplace/edit/ad=" + adID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(adUpdate)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenChangeAd_AndContentInvalid_thenReturnBadRequest() throws Exception {
        UpdatedAdvirtesementDTO adUpdate = new UpdatedAdvirtesementDTO(06, 2014, null, "Civic",null, "Diesel", 95874, 1500,  120, "Black", 15500, null, null, "Aveiro");
        long adID = 12312312;

        given(advirtesementService.changeAd(anyLong(), any(UpdatedAdvirtesementDTO.class))).willThrow(InvalidInput.class);
        mockMvc.perform(put("/carmarketplace/edit/ad=" + adID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(adUpdate)))
                .andExpect(status().isBadRequest());
    }


    @Test
    void whenSetSold_thenReturnTrue() throws Exception {
        long adID = 1;

        given(advirtesementService.setSold(adID)).willReturn(true);
        mockMvc.perform(put("/carmarketplace/sold/ad="+adID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("true"))
                .andExpect(status().isOk());
    }

    @Test
    void whenSetSold_thenReturnFalse() throws Exception {
        long adID = 1123341248;

        given(advirtesementService.setSold(adID)).willReturn(false);
        mockMvc.perform(put("/carmarketplace/sold/ad="+adID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("false"))
                .andExpect(status().isOk());
    }

    @Test
    void whendeleteAd_thenReturnNothing() throws Exception {
        long adID = 1;

        mockMvc.perform(delete("/carmarketplace/delete/ad="+adID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetUserSells_thenReturnListAdvirtesementSearchDTO() throws Exception {
        AdvirtesementSearchDTO adDTO1= new AdvirtesementSearchDTO(1, 1, "carolina", "Car", true, "Honda", "Civic", null, 19500, null, "Braga");
        AdvirtesementSearchDTO adDTO2= new AdvirtesementSearchDTO(2, 1, "carolina", "Car", true, "Seat", "Ibiza", null, 17500, null, "Aveiro");

        List<AdvirtesementSearchDTO> sellCar= new ArrayList<>(Arrays.asList(adDTO1, adDTO2));

        given(advirtesementService.getUserSells(any())).willReturn(sellCar);
        mockMvc.perform(get("/carmarketplace/user="+ adDTO1.getUserName()+"/sells")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].anuncioID", is(1)))
                .andExpect(jsonPath("$[0].userName", is(adDTO1.getUserName())))
                .andExpect(jsonPath("$[0].type", is(adDTO1.getType())))
                .andExpect(jsonPath("$[0].brand", is(adDTO1.getBrand())))
                .andExpect(jsonPath("$[0].location", is(adDTO1.getLocation())))
                .andExpect(jsonPath("$[1].anuncioID", is(2)))
                .andExpect(jsonPath("$[1].userName", is(adDTO2.getUserName())))
                .andExpect(jsonPath("$[1].type", is(adDTO2.getType())))
                .andExpect(jsonPath("$[1].brand", is(adDTO2.getBrand())))
                .andExpect(jsonPath("$[1].location", is(adDTO2.getLocation())));
    }

    @Test
    void whenGetUserSells_AndInvalidBodyParameters_thenReturnInternalServerError() throws Exception {
        String username = "blabla";

        given(advirtesementService.getUserSells(any())).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(get("/carmarketplace/user= "+ username + "/sells")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenGetFilterAd_thenReturnListAdvirtesementSearch() throws Exception {
        String type = "Car";
        String brand = "Honda";
        float price = 20000;

        AdvirtesementSearchDTO adDTO1= new AdvirtesementSearchDTO(1, 1, "carolina", "Car", true, "Honda", "Civic", null, 19500, null, "Braga");
        AdvirtesementSearchDTO adDTO2= new AdvirtesementSearchDTO(2, 1, "carolina", "Car", true, "Honda", "Jazz", null, 17500, null, "Aveiro");

        List<AdvirtesementSearchDTO> filterAd= new ArrayList<>(Arrays.asList(adDTO1, adDTO2));

        given(advirtesementService.getFilterAd(anyString(), anyString(), anyString(), anyString(), anyFloat())).willReturn(filterAd);
        mockMvc.perform(get("/carmarketplace/ads/type="+type+"/brand="+brand+"/model=/fuel=/price="+price)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].anuncioID", is(1)))
                .andExpect(jsonPath("$[0].userName", is(adDTO1.getUserName())))
                .andExpect(jsonPath("$[0].type", is(adDTO1.getType())))
                .andExpect(jsonPath("$[0].brand", is(adDTO1.getBrand())))
                .andExpect(jsonPath("$[0].location", is(adDTO1.getLocation())))
                .andExpect(jsonPath("$[1].anuncioID", is(2)))
                .andExpect(jsonPath("$[1].userName", is(adDTO2.getUserName())))
                .andExpect(jsonPath("$[1].type", is(adDTO2.getType())))
                .andExpect(jsonPath("$[1].brand", is(adDTO2.getBrand())))
                .andExpect(jsonPath("$[1].location", is(adDTO2.getLocation())));
    }

    @Test
    void whenGetFilterAd_AndContentInvalid_thenReturnBadRequest() throws Exception {
        String type = "Car";
        String brand = "Honda";
        float price = 20000;

        given(advirtesementService.getFilterAd(anyString(), anyString(), anyString(), anyString(), anyFloat())).willThrow(InvalidInput.class);
        mockMvc.perform(get("/carmarketplace/ads/type="+type+"/brand="+brand+"/model=/fuel=/price=")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}