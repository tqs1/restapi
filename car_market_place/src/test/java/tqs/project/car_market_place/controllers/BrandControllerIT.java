package tqs.project.car_market_place.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import tqs.project.car_market_place.CarMarketPlaceApplication;
import tqs.project.car_market_place.entities.Brand;
import tqs.project.car_market_place.entities.Model;
import tqs.project.car_market_place.repositories.BrandRepository;
import tqs.project.car_market_place.repositories.ModelRepository;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CarMarketPlaceApplication.class)
@AutoConfigureMockMvc
public class BrandControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private ModelRepository modelRepository;

    private Brand b1;
    private Brand b2;
    private Brand b3;
    private Brand b4;

    private Model m1;
    private Model m2;

    @BeforeEach
    void setUp() {
        brandRepository.deleteAll();
        modelRepository.deleteAll();

        b1 = new Brand("Mercedes", "Car");
        b2 = new Brand("BMW", "Car");
        b3 = new Brand("Honda", "Moto");
        b4 = new Brand("Yamaha", "Moto");
        brandRepository.save(b1);
        brandRepository.save(b2);
        brandRepository.save(b3);
        brandRepository.save(b4);
    }

    @Test
    void whenGetCarsBrand_thenReturnArrayJSON() throws Exception{
        mockMvc.perform(get("/carmarketplace/carBrands")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].brand", is("Mercedes")))
                .andExpect(jsonPath("$[1].brand", is("BMW")));
    }

    @Test
    void whenGetMotosBrand_thenReturnArrayJSON() throws Exception{
        mockMvc.perform(get("/carmarketplace/motoBrands")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].brand", is("Honda")))
                .andExpect(jsonPath("$[1].brand", is("Yamaha")));
    }

    @Test
    void whenGetModelsByBrandID_AndBrandIDIsValid_thenReturnList() throws Exception{
        long brandID= b1.getBrandID();
        m1 = new Model("Classe A", b1);
        m2 = new Model("Classe B", b1);
        modelRepository.save(m1);
        modelRepository.save(m2);

        mockMvc.perform(get("/carmarketplace/models/brand=" + brandID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", is("Classe A")))
                .andExpect(jsonPath("$[1]", is("Classe B")));
    }

    @Test
    void whenGetModelsByBrandID_AndBrandIDIsInvalid_thenReturnEmptyList() throws Exception{
        long brandID=10004534;

        mockMvc.perform(get("/carmarketplace/models/brand=" + brandID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

}
