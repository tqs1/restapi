package tqs.project.car_market_place.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import tqs.project.car_market_place.exceptions.InvalidBodyParameters;
import tqs.project.car_market_place.exceptions.InvalidInput;
import tqs.project.car_market_place.dto.UsersDTO;
import tqs.project.car_market_place.entities.Users;
import tqs.project.car_market_place.services.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static tqs.project.car_market_place.ToJSON.ToJson;

@WebMvcTest(UsersController.class)
class UsersControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService usersService;

    @Test
    void whenGetAllUsers_thenReturnArrayJSON() throws Exception {
        List<String> usernames= new ArrayList<>(Arrays.asList("carinaneves", "magalhaes"));

        given(usersService.getUsers()).willReturn(usernames);
        mockMvc.perform(get("/carmarketplace/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", is("carinaneves")))
                .andExpect(jsonPath("$[1]", is("magalhaes")));
    }

    @Test
    void whenGetAllEmails_thenReturnArrayJSON() throws Exception{
        List<String> emails = new ArrayList<>(Arrays.asList("carina@ua.pt", "magalhaes@ua.pt"));

        given(usersService.getEmails()).willReturn(emails);
        mockMvc.perform(get("/carmarketplace/emails")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", is("carina@ua.pt")))
                .andExpect(jsonPath("$[1]", is("magalhaes@ua.pt")));
    }

    @Test
    void whenNewValidUserSignIn_thenUserAdded() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carinaneves", "carina@ua.pt", "Carina", "123Carina", null, null);

        given(usersService.addNewUser(any(UsersDTO.class))).willReturn(new Users(usersDTO));
        mockMvc.perform(post("/carmarketplace/newUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(content().string("carinaneves"))
                .andExpect(status().isOk());
    }

    @Test
    void whenNewUserSignIn_AndInvalidBodyParameters_thenReturnInternalServerError() throws Exception {
        UsersDTO usersDTO = new UsersDTO("null", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");

        given(usersService.addNewUser(any(UsersDTO.class))).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(post("/carmarketplace/newUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenNewUserSignIn_AndContentInvalid_thenReturnBadRequest() throws Exception {

        given(usersService.addNewUser(any(UsersDTO.class))).willThrow(InvalidInput.class);
        mockMvc.perform(post("/carmarketplace/newUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson("")))
                .andExpect(status().isBadRequest());
    }


    @Test
    void whenGetUsersPassword_thenReturnPassword() throws Exception{
        Users u1 = new Users("magalhaes", "magalhaes@gmail.com", "Magalhães", "123magalhaes", null, null);
        String pass = "123magalhaes";

        given(usersService.getPassword(u1.getUsername())).willReturn(u1.getPassword());
        mockMvc.perform(get("/carmarketplace/user=" + u1.getUsername() + "/password")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(pass))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetPassword_UsernameNotFound_thenReturnEmptyString() throws Exception{
        String username  = "blabla";

        given(usersService.getPassword(username)).willReturn("");
        mockMvc.perform(get("/carmarketplace/user=" + username + "/password")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(""))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetUsersEmail_thenReturnEmail() throws Exception{
        Users u1 = new Users("magalhaes", "magalhaes@gmail.com", "Magalhães", "123magalhaes", null, null);
        String email = "magalhaes@gmail.com";

        given(usersService.getEmail(u1.getUsername())).willReturn(u1.getEmail());
        mockMvc.perform(get("/carmarketplace/user=" + u1.getUsername() + "/email")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(email))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetEmail_UsernameNotFound_thenReturnEmptyString() throws Exception{
        String username  = "blabla";

        given(usersService.getEmail(username)).willReturn("");
        mockMvc.perform(get("/carmarketplace/user=" + username + "/email")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(""))
                .andExpect(status().isOk());
    }


    @Test
    void whenGetUsersContact_thenReturnContact() throws Exception{
        Users u1 = new Users("magalhaes", "magalhaes@gmail.com", "Magalhães", "123magalhaes", null, "919191919");
        String contact = "919191919";

        given(usersService.getContact(u1.getUsername())).willReturn(u1.getContact());
        mockMvc.perform(get("/carmarketplace/user=" + u1.getUsername() + "/contact")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(contact))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetContact_UsernameNotFound_thenReturnEmptyString() throws Exception{
        String username  = "blabla";

        given(usersService.getContact(username)).willReturn("");
        mockMvc.perform(get("/carmarketplace/user=" + username + "/contact")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(""))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersUsername_thenReturnUserDTO() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolinaP", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "carolina";

        given(usersService.changeUsername(any(), any())).willReturn(usersDTO);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/username")
                .contentType(MediaType.APPLICATION_JSON)
                .content(usersDTO.getUsername()))
                .andExpect(jsonPath("$.username", is(usersDTO.getUsername())))
                .andExpect(jsonPath("$.email", is(usersDTO.getEmail())))
                .andExpect(jsonPath("$.name", is(usersDTO.getName())))
                .andExpect(jsonPath("$.password", is(usersDTO.getPassword())))
                .andExpect(jsonPath("$.photo", is(usersDTO.getPhoto())))
                .andExpect(jsonPath("$.contact", is(usersDTO.getContact())))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersUsername_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "blabla";

        given(usersService.changeUsername(any(), any())).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/username")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenChangeUsersUsername_AndContentInvalid_thenReturnBadRequest() throws Exception {
        String username = "carolina";

        given(usersService.changeUsername(any(), any())).willThrow(InvalidInput.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/username")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson("")))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenChangeUsersPassword_thenReturnUserDTO() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "carolina";

        given(usersService.changePassword(any(), any())).willReturn(usersDTO);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(jsonPath("$.username", is(usersDTO.getUsername())))
                .andExpect(jsonPath("$.email", is(usersDTO.getEmail())))
                .andExpect(jsonPath("$.name", is(usersDTO.getName())))
                .andExpect(jsonPath("$.password", is(usersDTO.getPassword())))
                .andExpect(jsonPath("$.photo", is(usersDTO.getPhoto())))
                .andExpect(jsonPath("$.contact", is(usersDTO.getContact())))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersPassword_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "blabla";

        given(usersService.changePassword(any(), any())).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenChangeUsersPassword_AndContentInvalid_thenReturnBadRequest() throws Exception {
        String username = "carolina";

        given(usersService.changePassword(any(), any())).willThrow(InvalidInput.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson("")))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenChangeUsersEmail_thenReturnUserDTO() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "carolina";

        given(usersService.changeEmail(any(), any())).willReturn(usersDTO);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(jsonPath("$.username", is(usersDTO.getUsername())))
                .andExpect(jsonPath("$.email", is(usersDTO.getEmail())))
                .andExpect(jsonPath("$.name", is(usersDTO.getName())))
                .andExpect(jsonPath("$.password", is(usersDTO.getPassword())))
                .andExpect(jsonPath("$.photo", is(usersDTO.getPhoto())))
                .andExpect(jsonPath("$.contact", is(usersDTO.getContact())))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersEmail_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "blabla";

        given(usersService.changeEmail(any(), any())).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenChangeUsersEmail_AndContentInvalid_thenReturnBadRequest() throws Exception {
        String username = "carolina";

        given(usersService.changeEmail(any(), any())).willThrow(InvalidInput.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson("")))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenChangeUsersPhoto_thenReturnUserDTO() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "carolina";

        given(usersService.changePhoto(any(), any())).willReturn(usersDTO);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/photo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(jsonPath("$.username", is(usersDTO.getUsername())))
                .andExpect(jsonPath("$.email", is(usersDTO.getEmail())))
                .andExpect(jsonPath("$.name", is(usersDTO.getName())))
                .andExpect(jsonPath("$.password", is(usersDTO.getPassword())))
                .andExpect(jsonPath("$.photo", is(usersDTO.getPhoto())))
                .andExpect(jsonPath("$.contact", is(usersDTO.getContact())))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersPhoto_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "blabla";

        given(usersService.changePhoto(any(), any())).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/photo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenChangeUsersPhoto_AndContentInvalid_thenReturnBadRequest() throws Exception {
        String username = "carolina";

        given(usersService.changePhoto(any(), any())).willThrow(InvalidInput.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/photo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson("")))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenChangeUsersContact_thenReturnUserDTO() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "carolina";

        given(usersService.changeContact(any(), any())).willReturn(usersDTO);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/contact")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(jsonPath("$.username", is(usersDTO.getUsername())))
                .andExpect(jsonPath("$.email", is(usersDTO.getEmail())))
                .andExpect(jsonPath("$.name", is(usersDTO.getName())))
                .andExpect(jsonPath("$.password", is(usersDTO.getPassword())))
                .andExpect(jsonPath("$.photo", is(usersDTO.getPhoto())))
                .andExpect(jsonPath("$.contact", is(usersDTO.getContact())))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersContact_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        UsersDTO usersDTO = new UsersDTO("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");
        String username = "blabla";

        given(usersService.changeContact(any(), any())).willThrow(InvalidBodyParameters.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/contact")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(usersDTO)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void whenChangeUsersContact_AndContentInvalid_thenReturnBadRequest() throws Exception {
        String username = "carolina";

        given(usersService.changeContact(any(), any())).willThrow(InvalidInput.class);
        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/contact")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson("")))
                .andExpect(status().isBadRequest());
    }
}