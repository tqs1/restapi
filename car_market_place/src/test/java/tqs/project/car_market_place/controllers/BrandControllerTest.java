package tqs.project.car_market_place.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import tqs.project.car_market_place.dto.BrandDTO;
import tqs.project.car_market_place.exceptions.InvalidBrandID;
import tqs.project.car_market_place.services.BrandService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BrandController.class)
class BrandControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    BrandService brandService;

    @Test
    void whenGetCarsBrand_thenReturnArrayJSON() throws Exception{
        BrandDTO brandDTO1 = new BrandDTO("Honda");
        BrandDTO brandDTO2 = new BrandDTO("Mercedes");
        List<BrandDTO> carBrand= new ArrayList<>(Arrays.asList(brandDTO1, brandDTO2));

        given(brandService.getCarBrands()).willReturn(carBrand);
        mockMvc.perform(get("/carmarketplace/carBrands")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].brand", is("Honda")))
                .andExpect(jsonPath("$[1].brand", is("Mercedes")));
    }

    @Test
    void whenGetMotosBrand_thenReturnArrayJSON() throws Exception{
        BrandDTO brandDTO1 = new BrandDTO("Honda");
        BrandDTO brandDTO2 = new BrandDTO("Yamaha");
        List<BrandDTO> motoBrand= new ArrayList<>(Arrays.asList(brandDTO1, brandDTO2));

        given(brandService.getMotoBrands()).willReturn(motoBrand);
        mockMvc.perform(get("/carmarketplace/motoBrands")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].brand", is("Honda")))
                .andExpect(jsonPath("$[1].brand", is("Yamaha")));
    }

    @Test
    void whenGetModelsByBrandID_AndBrandIDIsValid_thenReturnList() throws Exception{
        long brandID=1;

        List<String> models = new ArrayList<>(Arrays.asList("civic", "typer"));

        given(brandService.getModelsByBrandID(any())).willReturn(models);
        mockMvc.perform(get("/carmarketplace/models/brand="+brandID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", is("civic")))
                .andExpect(jsonPath("$[1]", is("typer")));
    }

    @Test
    void whenGetModelsByBrandID_AndBrandIDIsInvalid_thenReturnNotFound() throws Exception{
        long brandID=10004534;
        given(brandService.getModelsByBrandID(any())).willThrow(InvalidBrandID.class);
        mockMvc.perform(get("/carmarketplace/models/brand="+brandID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }
}