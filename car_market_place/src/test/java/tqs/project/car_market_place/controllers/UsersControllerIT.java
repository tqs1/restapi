package tqs.project.car_market_place.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import tqs.project.car_market_place.CarMarketPlaceApplication;
import tqs.project.car_market_place.entities.Users;
import tqs.project.car_market_place.repositories.UsersRepository;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static tqs.project.car_market_place.ToJSON.ToJson;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CarMarketPlaceApplication.class)
@AutoConfigureMockMvc
public class UsersControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UsersRepository usersRepository;

    private Users user1;
    private Users user2;

    @BeforeEach
    void setUp() {
        usersRepository.deleteAll();
        user1 = new Users("carinaneves", "carina@ua.pt", "Carina", "123Carina", null, null);
        user2 = new Users("magalhaes", "magalhaes@ua.pt", "Magalhães", "Magalhaes456", null, "913913913");
        usersRepository.save(user1);
        usersRepository.save(user2);
    }

    @Test
    void whenGetAllUsers_thenReturnArrayJSON() throws Exception {
        mockMvc.perform(get("/carmarketplace/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", is("carinaneves")))
                .andExpect(jsonPath("$[1]", is("magalhaes")));
    }

    @Test
    void whenGetAllEmails_thenReturnArrayJSON() throws Exception{
        mockMvc.perform(get("/carmarketplace/emails")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", is("carina@ua.pt")))
                .andExpect(jsonPath("$[1]", is("magalhaes@ua.pt")));
    }

    @Test
    void whenNewValidUserSignIn_thenUserAdded() throws Exception {
        Users user3 = new Users("carolina", "carolina@ua.pt", "Carolina Pereira", "123carolina", null, "123456789");

        mockMvc.perform(post("/carmarketplace/newUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson(user3)))
                .andExpect(content().string("carolina"))
                .andExpect(status().isOk());
    }

    @Test
    void whenNewUserSignIn_AndContentInvalid_thenReturnBadRequest() throws Exception {
        mockMvc.perform(post("/carmarketplace/newUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJson("")))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenGetUsersPassword_thenReturnPassword() throws Exception{
        mockMvc.perform(get("/carmarketplace/user=" + user2.getUsername() + "/password")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(user2.getPassword()))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetPassword_UsernameNotFound_thenReturnError() throws Exception{
        String username  = "blabla";

        mockMvc.perform(get("/carmarketplace/user=" + username + "/password")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void whenGetUsersEmail_thenReturnEmail() throws Exception{
        mockMvc.perform(get("/carmarketplace/user=" + user1.getUsername() + "/email")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(user1.getEmail()))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetEmail_UsernameNotFound_thenReturnError() throws Exception{
        String username  = "blabla";

        mockMvc.perform(get("/carmarketplace/user=" + username + "/email")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void whenGetUsersContact_thenReturnContact() throws Exception{
        mockMvc.perform(get("/carmarketplace/user=" + user2.getUsername() + "/contact")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(user2.getContact()))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetContact_UsernameNotFound_thenReturnError() throws Exception {
        String username = "blabla";

        mockMvc.perform(get("/carmarketplace/user=" + username + "/contact")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void whenChangeUsersUsername_thenReturnUserDTO() throws Exception {
        String newUsername = "carinaneves15";

        mockMvc.perform(put("/carmarketplace/edit/user=" + user1.getUsername() + "/username")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newUsername))
                .andExpect(jsonPath("$.username", is(newUsername)))
                .andExpect(jsonPath("$.email", is(user1.getEmail())))
                .andExpect(jsonPath("$.name", is(user1.getName())))
                .andExpect(jsonPath("$.password", is(user1.getPassword())))
                .andExpect(jsonPath("$.photo", is(user1.getPhoto())))
                .andExpect(jsonPath("$.contact", is(user1.getContact())))
                .andExpect(status().isOk());
    }


    @Test
    void whenChangeUsersUsername_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        String username = "blabla";
        String newUsername = "carinaneves15";

        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/username")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newUsername))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenChangeUsersUsername_AndContentInvalid_thenReturnBadRequest() throws Exception {
        mockMvc.perform(put("/carmarketplace/edit/user=" + user1.getUsername() + "/username")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenChangeUsersPassword_thenReturnUserDTO() throws Exception {
        String newPassword = "jrsrm";

        mockMvc.perform(put("/carmarketplace/edit/user=" + user2.getUsername() + "/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newPassword))
                .andExpect(jsonPath("$.username", is(user2.getUsername())))
                .andExpect(jsonPath("$.email", is(user2.getEmail())))
                .andExpect(jsonPath("$.name", is(user2.getName())))
                .andExpect(jsonPath("$.password", is(newPassword)))
                .andExpect(jsonPath("$.photo", is(user2.getPhoto())))
                .andExpect(jsonPath("$.contact", is(user2.getContact())))
                .andExpect(status().isOk());
    }


    @Test
    void whenChangeUsersPassword_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        String username = "blabla";
        String newPassword = "jrsrm";

        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newPassword))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenChangeUsersPassword_AndContentInvalid_thenReturnBadRequest() throws Exception {
        mockMvc.perform(put("/carmarketplace/edit/user=" + user2.getUsername() + "/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenChangeUsersEmail_thenReturnUserDTO() throws Exception {
        String newEmail = "carinan@ua.pt";

        mockMvc.perform(put("/carmarketplace/edit/user=" + user1.getUsername() + "/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newEmail))
                .andExpect(jsonPath("$.username", is(user1.getUsername())))
                .andExpect(jsonPath("$.email", is(newEmail)))
                .andExpect(jsonPath("$.name", is(user1.getName())))
                .andExpect(jsonPath("$.password", is(user1.getPassword())))
                .andExpect(jsonPath("$.photo", is(user1.getPhoto())))
                .andExpect(jsonPath("$.contact", is(user1.getContact())))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersEmail_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        String username = "blabla";
        String newEmail = "carinan@ua.pt";

        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newEmail))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenChangeUsersEmail_AndContentInvalid_thenReturnBadRequest() throws Exception {
        mockMvc.perform(put("/carmarketplace/edit/user=" + user1.getUsername() + "/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenChangeUsersPhoto_thenReturnUserDTO() throws Exception {
        String newPhoto = "abcdefg";

        mockMvc.perform(put("/carmarketplace/edit/user=" + user2.getUsername() + "/photo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newPhoto))
                .andExpect(jsonPath("$.username", is(user2.getUsername())))
                .andExpect(jsonPath("$.email", is(user2.getEmail())))
                .andExpect(jsonPath("$.name", is(user2.getName())))
                .andExpect(jsonPath("$.password", is(user2.getPassword())))
                .andExpect(jsonPath("$.photo", is(newPhoto)))
                .andExpect(jsonPath("$.contact", is(user2.getContact())))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersPhoto_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        String username = "blabla";
        String newPhoto = "abcdefg";

        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/photo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newPhoto))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenChangeUsersPhoto_AndContentInvalid_thenReturnBadRequest() throws Exception {
        mockMvc.perform(put("/carmarketplace/edit/user=" + user2.getUsername() + "/photo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenChangeUsersContact_thenReturnUserDTO() throws Exception {
        String newContact = "914914914";

        mockMvc.perform(put("/carmarketplace/edit/user=" + user1.getUsername() + "/contact")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newContact))
                .andExpect(jsonPath("$.username", is(user1.getUsername())))
                .andExpect(jsonPath("$.email", is(user1.getEmail())))
                .andExpect(jsonPath("$.name", is(user1.getName())))
                .andExpect(jsonPath("$.password", is(user1.getPassword())))
                .andExpect(jsonPath("$.photo", is(user1.getPhoto())))
                .andExpect(jsonPath("$.contact", is(newContact)))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersContact_AndInvalidUsername_thenReturnInternalServerError() throws Exception {
        String username = "blabla";
        String newContact = "914914914";

        mockMvc.perform(put("/carmarketplace/edit/user=" + username + "/contact")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newContact))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenChangeUsersContact_AndContentInvalid_thenReturnBadRequest() throws Exception {
        mockMvc.perform(put("/carmarketplace/edit/user=" + user1.getUsername() + "/contact")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isBadRequest());
    }
}
