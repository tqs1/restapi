package tqs.project.car_market_place.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tqs.project.car_market_place.dto.UsersDTO;
import tqs.project.car_market_place.entities.Users;
import tqs.project.car_market_place.services.UserService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/carmarketplace")
public class UsersController {

    @Autowired
    private UserService utilizadorService;
    //sign up
    @GetMapping("/users")
    public List<String> getUsers() {
        return utilizadorService.getUsers();
    }

    @GetMapping("/emails")
    public List<String> getEmails() {
        return utilizadorService.getEmails();
    }

    @PostMapping("/newUser")
    public String addNewUser(@RequestBody UsersDTO utilizadorDTO) {
        Users newUser = utilizadorService.addNewUser(utilizadorDTO);
        return newUser.getUsername();
    }

    //settings
    @GetMapping("/user={username}/password")
    public String getPassword(@PathVariable(value = "username") String username) {
        return utilizadorService.getPassword(username);
    }

    @GetMapping("/user={username}/email")
    public String getEmail(@PathVariable(value = "username") String username) {
        return utilizadorService.getEmail(username);
    }

    @GetMapping("/user={username}/contact")
    public String getContact(@PathVariable(value = "username") String username) {
        return utilizadorService.getContact(username);
    }

    @PutMapping("/edit/user={username}/username")
    public UsersDTO changeUsername(@PathVariable("username") String username, @RequestBody String newUsername) {
        return utilizadorService.changeUsername(username, newUsername);
    }

    @PutMapping("/edit/user={username}/password")
    public UsersDTO changePassword(@PathVariable("username") String username, @RequestBody String newPassword) {
        return utilizadorService.changePassword(username, newPassword);
    }

    @PutMapping("/edit/user={username}/email")
    public UsersDTO changeEmail(@PathVariable("username") String username, @RequestBody String newEmail) {
        return utilizadorService.changeEmail(username, newEmail);
    }

    @PutMapping("/edit/user={username}/photo")
    public UsersDTO changePhoto(@PathVariable("username") String username, @RequestBody String newPhoto) {
        return utilizadorService.changePhoto(username, newPhoto);
    }

    @PutMapping("/edit/user={username}/contact")
    public UsersDTO changeContact(@PathVariable("username") String username, @RequestBody String newContact) {
        return utilizadorService.changeContact(username, newContact);
    }



}
