package tqs.project.car_market_place.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tqs.project.car_market_place.dto.BrandDTO;
import tqs.project.car_market_place.services.BrandService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/carmarketplace")
public class BrandController {

    @Autowired
    private BrandService brandService;

    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    @GetMapping(value="/carBrands")
    public List<BrandDTO> getCarBrands(){
        return brandService.getCarBrands();
    }

    @GetMapping(value="/motoBrands")
    public List<BrandDTO> getMotoBrands(){
        return brandService.getMotoBrands();
    }

    @GetMapping(value="/models/brand={brandID}")
    public List<String> getModelsByBrandID(@PathVariable("brandID") Long brandID) {
        return brandService.getModelsByBrandID(brandID);
    }

}
