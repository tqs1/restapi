package tqs.project.car_market_place.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tqs.project.car_market_place.dto.*;
import tqs.project.car_market_place.entities.Advirtesement;
import tqs.project.car_market_place.services.AdvirtesementService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/carmarketplace")
public class AdvirtesementController {

    @Autowired
    private AdvirtesementService advirtesementService;

    public AdvirtesementController(AdvirtesementService advirtesementService) {
        this.advirtesementService = advirtesementService;
    }

    @GetMapping(value="/ad={adID}")
    public AdvirtesementDTO getAdByID(@PathVariable("adID") Long id) {
        return advirtesementService.getAdByID(id);
    }

    @GetMapping(value="/all/type={type}")
    public List<AdvirtesementSearchDTO> getAll(@PathVariable("type") String type){
        return advirtesementService.getAll(type);
    }

    @PostMapping(value = "/newAd")
    public Advirtesement addNewAd(@RequestBody NewAdvirtesementDTO newAdvirtesementDTO) {
        return advirtesementService.addNewAd(newAdvirtesementDTO);
    }

    @PutMapping(value = "/edit/ad={adID}")
    public AdvirtesementDTO changeAd(@PathVariable("adID") long adID, @RequestBody UpdatedAdvirtesementDTO updatedAdvirtesementDTO) {
        return advirtesementService.changeAd(adID, updatedAdvirtesementDTO);
    }

    @PutMapping(value = "/sold/ad={adID}")
    public boolean setSold(@PathVariable("adID") long adID) {
        return advirtesementService.setSold(adID);
    }

    @DeleteMapping(value = "delete/ad={adID}")
    public int deleteAd(@PathVariable("adID") long adID) {
        return advirtesementService.deleteAd(adID);
    }

    @GetMapping("/user={username}/sells")
    public List<AdvirtesementSearchDTO> getUserSells(@PathVariable(value = "username") String username) {
        return advirtesementService.getUserSells(username);
    }

    @GetMapping(value = "/ads/type={type}/brand={brand}/model={model}/fuel={fuel}/price={price}")
    public List<AdvirtesementSearchDTO> getFilterAd (@PathVariable String type,
                                                     @PathVariable String brand,
                                                     @PathVariable String model,
                                                     @PathVariable String fuel,
                                                     @PathVariable float price){
        return advirtesementService.getFilterAd(type, brand, model, fuel, price);
    }



}
