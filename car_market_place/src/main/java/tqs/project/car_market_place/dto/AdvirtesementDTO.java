package tqs.project.car_market_place.dto;

import tqs.project.car_market_place.entities.Advirtesement;
import tqs.project.car_market_place.entities.Users;

import java.sql.Timestamp;

public class AdvirtesementDTO {

    private long userID;
    private String userName;
    private String contacto;
    private String email;
    private String type;
    private boolean state;
    private Timestamp time ;
    private int month;
    private int year;
    private String brand;
    private String model;
    private String version;
    private String fuel;
    private int kms;
    private int cylinder;
    private int power;
    private String color;
    private float price;
    private String image;
    private String annotation;
    private String location;

    public AdvirtesementDTO(long userID, String userName, String contacto, String email, String type, boolean state, Timestamp time, int month, int year, String brand, String model, String version, String fuel, int kms, int cylinder, int power, String color, float price, String image, String annotation, String location) {
        this.userID = userID;
        this.userName = userName;
        this.contacto = contacto;
        this.email = email;
        this.type = type;
        this.state = state;
        this.time = time;
        this.month = month;
        this.year = year;
        this.brand = brand;
        this.model = model;
        this.version = version;
        this.fuel = fuel;
        this.kms = kms;
        this.cylinder = cylinder;
        this.power = power;
        this.color = color;
        this.price = price;
        this.image = image;
        this.annotation = annotation;
        this.location = location;
    }

    public AdvirtesementDTO(Advirtesement car, Users user) {
        this.userID = user.getUserID();
        this.userName = user.getName();
        this.contacto = user.getContact();
        this.email = user.getEmail();
        this.type = car.getType();
        this.state = car.isState();
        this.time = car.getTime();
        this.month = car.getMonth();
        this.year = car.getYear();
        this.brand = car.getBrand();
        this.model = car.getModel();
        this.version = car.getVersion();
        this.fuel = car.getFuel();
        this.kms = car.getKms();
        this.cylinder = car.getCylinder();
        this.power = car.getPower();
        this.color = car.getColor();
        this.price = car.getPrice();
        this.image = car.getImage();
        this.annotation = car.getAnnotations();
        this.location = car.getLocation();
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getType() {
        return type;
    }

    public boolean isState() {
        return state;
    }

    public Timestamp getTime() {
        return time;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getVersion() {
        return version;
    }

    public String getFuel() {
        return fuel;
    }

    public int getKms() {
        return kms;
    }

    public int getCylinder() {
        return cylinder;
    }

    public int getPower() {
        return power;
    }

    public String getColor() {
        return color;
    }

    public float getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public String getAnnotation() {
        return annotation;
    }

    public String getUserName() {
        return userName;
    }

    public String getContacto() {
        return contacto;
    }

    public String getEmail() {
        return email;
    }

    public String getLocation() {
        return location;
    }

}
