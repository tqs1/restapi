package tqs.project.car_market_place.dto;

public class UpdatedAdvirtesementDTO {
    private int month;
    private int year;
    private String brand;
    private String model;
    private String version;
    private String fuel;
    private int kms;
    private int cylinder;
    private int power;
    private String color;
    private float price;
    private String image;
    private String annotation;
    private String location;

    public UpdatedAdvirtesementDTO(int month, int year, String brand, String model, String version, String fuel, int kms, int cylinder, int power, String color, float price, String image, String annotation, String location) {
        this.month = month;
        this.year = year;
        this.brand = brand;
        this.model = model;
        this.version = version;
        this.fuel = fuel;
        this.kms = kms;
        this.cylinder = cylinder;
        this.power = power;
        this.color = color;
        this.price = price;
        this.image = image;
        this.annotation = annotation;
        this.location = location;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getVersion() {
        return version;
    }

    public String getFuel() {
        return fuel;
    }

    public int getKms() {
        return kms;
    }

    public int getCylinder() {
        return cylinder;
    }

    public int getPower() {
        return power;
    }

    public String getColor() {
        return color;
    }

    public float getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public String getAnnotation() {
        return annotation;
    }

    public String getLocation() {
        return location;
    }
}
