package tqs.project.car_market_place.dto;

import tqs.project.car_market_place.entities.Advirtesement;
import tqs.project.car_market_place.entities.Users;

public class AdvirtesementSearchDTO {

    private long anuncioID;
    private long userID;
    private String userName;
    private String type;
    private boolean state;
    private String brand;
    private String model;
    private String version;
    private float price;
    private String imagem;
    private String location;

    public AdvirtesementSearchDTO(long anuncioID, long userID, String userName, String type, boolean state, String brand, String model, String version, float price, String imagem, String location) {
        this.anuncioID = anuncioID;
        this.userID = userID;
        this.userName = userName;
        this.type = type;
        this.state = state;
        this.brand = brand;
        this.model = model;
        this.version = version;
        this.price = price;
        this.imagem = imagem;
        this.location = location;
    }

    public AdvirtesementSearchDTO(Advirtesement car, Users user) {
        this.anuncioID = car.getAdID();
        this.userID = user.getUserID();
        this.userName = user.getName();
        this.type = car.getType();
        this.state = car.isState();
        this.brand = car.getBrand();
        this.model = car.getModel();
        this.version = car.getVersion();
        this.price = car.getPrice();
        this.imagem = car.getImage();
        this.location = car.getLocation();
    }

    public long getAnuncioID() {
        return anuncioID;
    }

    public void setAnuncioID(long anuncioID) {
        this.anuncioID = anuncioID;
    }

    public long getUserID() {
        return userID;
    }

    public String getType() {
        return type;
    }

    public boolean isState() {
        return state;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getVersion() {
        return version;
    }

    public float getPrice() {
        return price;
    }

    public String getUserName() {
        return userName;
    }

    public String getImagem() {
        return imagem;
    }

    public String getLocation() {
        return location;
    }
}
