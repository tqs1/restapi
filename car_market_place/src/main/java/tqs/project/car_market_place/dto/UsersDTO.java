package tqs.project.car_market_place.dto;


import tqs.project.car_market_place.entities.Users;

public class UsersDTO {
    private String username;
    private String email;
    private String name;
    private String password;
    private String photo;
    private String contact;

    public UsersDTO(String username, String email, String name, String password, String photo, String contact) {
        this.username = username;
        this.email = email;
        this.name = name;
        this.password = password;
        this.photo = photo;
        this.contact = contact;
    }

    public UsersDTO(Users u){
        this.username = u.getUsername();
        this.email = u.getEmail();
        this.name = u.getName();
        this.password = u.getPassword();
        this.photo = u.getPhoto();
        this.contact = u.getContact();
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getPhoto() {
        return photo;
    }

    public String getContact() {
        return contact;
    }
}
