package tqs.project.car_market_place.dto;

import tqs.project.car_market_place.entities.Brand;

public class BrandDTO {
    private long brandID;
    private String brand;

    public BrandDTO(Brand brand) {
        this.brandID = brand.getBrandID();
        this.brand = brand.getBrand();
    }

    public BrandDTO(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public long getBrandID() {
        return brandID;
    }
}
