package tqs.project.car_market_place.dto;

import java.sql.Timestamp;

public class NewAdvirtesementDTO {
    private String username;
    private String type;
    private boolean state;
    private Timestamp time ;
    private int month;
    private int year;
    private String brand;
    private String model;
    private String version;
    private String fuel;
    private int kms;
    private int cylinder;
    private int power;
    private String color;
    private float price;
    private String image;
    private String annotation;
    private String location;

    public NewAdvirtesementDTO(String username, String type, boolean state, Timestamp time, int month, int year, String brand, String model, String version, String fuel, int kms, int cylinder, int power, String color, float price, String image, String annotation, String location) {
        this.username = username;
        this.type = type;
        this.state = state;
        this.time = time;
        this.month = month;
        this.year = year;
        this.brand = brand;
        this.model = model;
        this.version = version;
        this.fuel = fuel;
        this.kms = kms;
        this.cylinder = cylinder;
        this.power = power;
        this.color = color;
        this.price = price;
        this.image = image;
        this.annotation = annotation;
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public String getType() {
        return type;
    }

    public boolean isState() {
        return state;
    }

    public Timestamp getTime() {
        return time;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getVersion() {
        return version;
    }

    public String getFuel() {
        return fuel;
    }

    public int getKms() {
        return kms;
    }

    public int getCylinder() {
        return cylinder;
    }

    public int getPower() {
        return power;
    }

    public String getColor() {
        return color;
    }

    public float getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public String getAnnotation() {
        return annotation;
    }

    public String getLocation() {
        return location;
    }
}
