package tqs.project.car_market_place.dto;

import tqs.project.car_market_place.entities.Brand;
import tqs.project.car_market_place.entities.Model;

public class ModelDTO {

    private Brand brandID;
    private String model;

    public ModelDTO(Brand brandID, String model) {
        this.brandID = brandID;
        this.model = model;
    }

    public ModelDTO(Model model, Brand brand) {
        this.brandID = brand;
        this.model = model.getModel();
    }

    public Brand getBrandID() {
        return brandID;
    }

    public String getModel() {
        return model;
    }
}
