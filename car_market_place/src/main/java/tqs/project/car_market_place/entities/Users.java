package tqs.project.car_market_place.entities;


import tqs.project.car_market_place.dto.UsersDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "users")
public class Users {

    public Users() {
        //default contructor
    }

    public Users(String username, String email, String name, String password, String photo, String contact) {
        this.username = username;
        this.email = email;
        this.name = name;
        this.password = password;
        this.photo = photo;
        this.contact = contact;
    }

    public Users(UsersDTO usersDTO){
        this.username = usersDTO.getUsername();
        this.email = usersDTO.getEmail();
        this.name = usersDTO.getName();
        this.password = usersDTO.getPassword();
        this.photo = usersDTO.getPhoto();
        this.contact = usersDTO.getContact();
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userID;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "password")
    private String password;

    @Column(name = "photo")
    private String photo;

    @Column(name = "contact")
    private String contact;

    public long getUserID() {
        return userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}