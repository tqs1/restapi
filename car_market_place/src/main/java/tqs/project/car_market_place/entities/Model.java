package tqs.project.car_market_place.entities;


import tqs.project.car_market_place.dto.ModelDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "model")
public class Model {

    public Model() {
        //default contructor
    }

    public Model(String model, Brand brandID) {
        this.model = model;
        this.brandID = brandID;
    }

    public Model(ModelDTO modelDTO){
        this.model = modelDTO.getModel();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long modelID;

    @NotNull
    @Column(name = "model")
    private String model;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "brandID")
    private Brand brandID;

    public long getModelID() {
        return modelID;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Brand getBrandID() {
        return brandID;
    }

    public void setBrandID(Brand brandID) {
        this.brandID = brandID;
    }
}
