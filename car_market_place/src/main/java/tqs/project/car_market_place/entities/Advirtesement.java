package tqs.project.car_market_place.entities;

import tqs.project.car_market_place.dto.NewAdvirtesementDTO;

import java.sql.Timestamp;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "advirtesement")
public class Advirtesement {

    public Advirtesement() {
        //default contructor
    }

    public Advirtesement(String type, boolean state, Timestamp time, int month, int year, String brand, String model, String version, String fuel, int kms, int cylinder, int power, String color, float price, String image, String annotations, String location) {
        this.type = type;
        this.state = state;
        this.time = time;
        this.month = month;
        this.year = year;
        this.brand = brand;
        this.model = model;
        this.version = version;
        this.fuel = fuel;
        this.kms = kms;
        this.cylinder = cylinder;
        this.power = power;
        this.color = color;
        this.price = price;
        this.image = image;
        this.annotations = annotations;
        this.location = location;
    }

    public Advirtesement(NewAdvirtesementDTO advirtesement, Users users){
        this.userID = users;
        this.type = advirtesement.getType();
        this.state = advirtesement.isState();
        this.time = advirtesement.getTime();
        this.month = advirtesement.getMonth();
        this.year = advirtesement.getYear();
        this.brand = advirtesement.getBrand();
        this.model = advirtesement.getModel();
        this.version = advirtesement.getVersion();
        this.fuel = advirtesement.getFuel();
        this.kms = advirtesement.getKms();
        this.cylinder = advirtesement.getCylinder();
        this.power = advirtesement.getPower();
        this.color = advirtesement.getColor();
        this.price = advirtesement.getPrice();
        this.image = advirtesement.getImage();
        this.annotations = advirtesement.getAnnotation();
        this.location = advirtesement.getLocation();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long adID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userID")
    private Users userID;

    @NotNull
    @Column(name = "type")
    private String type;

    @NotNull
    @Column(name = "state")
    private boolean state;

    @NotNull
    @Column(name = "time")
    private Timestamp time;

    @NotNull
    @Column(name = "month")
    private int month;

    @NotNull
    @Column(name = "year")
    private int year;

    @NotNull
    @Column(name = "brand")
    private String brand;

    @NotNull
    @Column(name = "model")
    private String model;

    @Column(name = "version")
    private String version;

    @NotNull
    @Column(name = "fuel")
    private String fuel;

    @NotNull
    @Column(name = "kms")
    private int kms;

    @NotNull
    @Column(name = "cylinder")
    private int cylinder;

    @NotNull
    @Column(name = "power")
    private int power;

    @NotNull
    @Column(name = "color")
    private String color;

    @NotNull
    @Column(name = "price")
    private float price;

    @Column(name = "image")
    private String image;

    @Column(name = "annotations")
    private String annotations;

    @Column(name = "location")
    private String location;

    public long getAdID() {
        return adID;
    }

    public Users getUserID() {
        return userID;
    }

    public void setUserID(Users userID) {
        this.userID = userID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public int getKms() {
        return kms;
    }

    public void setKms(int kms) {
        this.kms = kms;
    }

    public int getCylinder() {
        return cylinder;
    }

    public void setCylinder(int cylinder) {
        this.cylinder = cylinder;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotiations) {
        this.annotations = annotiations;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

