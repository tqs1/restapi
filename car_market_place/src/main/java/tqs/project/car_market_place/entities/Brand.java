package tqs.project.car_market_place.entities;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "brand")
public class Brand {

    public Brand() {
            //default contructor
    }

    public Brand(String brand, String type) {
        this.brand = brand;
        this.type = type;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long brandID;


    @Column(name = "brand")
    @NotNull
    private String brand;


    @Column(name = "type")
    @NotNull
    private String type;

    public long getBrandID() {
        return brandID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
