package tqs.project.car_market_place.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tqs.project.car_market_place.entities.Model;

@Repository
public interface ModelRepository extends JpaRepository<Model,Long> {
}
