package tqs.project.car_market_place.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tqs.project.car_market_place.entities.Brand;

import java.util.List;

@Repository
public interface BrandRepository extends JpaRepository<Brand,Long>  {

    @Query(value = "Select * from brand where type = 'Car'", nativeQuery = true)
    List<Brand> getCarBrands();

    @Query(value = "Select * from brand where type = 'Moto'", nativeQuery = true)
    List<Brand> getMotoBrands();

    @Query(value = "Select model from model where brandid = :id", nativeQuery = true)
    List<String> getModelsByBrandID(@Param("id") Long id);

}
