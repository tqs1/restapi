package tqs.project.car_market_place.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tqs.project.car_market_place.entities.Users;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {

    @Query(value ="Select username from users", nativeQuery = true)
    List<String> getUsers();

    @Query(value ="Select email from users", nativeQuery = true)
    List<String> getEmails();

    @Query(value ="Select password from users where username =:username", nativeQuery = true)
    String getPassword(@Param("username") String username);

    @Query(value ="Select email from users where username =:username", nativeQuery = true)
    String getEmail(@Param("username") String username);

    @Query(value ="Select contact from users where username =:username", nativeQuery = true)
    String getContact(@Param("username") String username);

    Users findByUsername(String username);

    @Query(value = "Select userID from users where username =:username", nativeQuery = true)
    long findIdByUsername(@Param("username") String username);

}
