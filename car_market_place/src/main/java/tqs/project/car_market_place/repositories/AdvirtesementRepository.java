package tqs.project.car_market_place.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tqs.project.car_market_place.entities.Advirtesement;

import java.util.List;

@Repository
public interface AdvirtesementRepository extends JpaRepository<Advirtesement,Long> {

    @Query(value = "Select * from advirtesement where type =:type and state = true", nativeQuery = true)
    List<Advirtesement> getAll(String type);

    @Query(value = "Select * from advirtesement where adID=:id", nativeQuery = true)
    Advirtesement findByID(@Param("id") Long id);

    @Modifying
    @Transactional
    @Query(value = "Delete from advirtesement where adID=:id", nativeQuery = true)
    int delete(@Param("id") long id);

    @Query(value = "Select * from advirtesement where userID=:id", nativeQuery = true)
    List<Advirtesement> getUserSells(@Param("id") long id);

    @Query (value = "Select * from advirtesement where (:type= '' or type=:type) and " +
            "(:brand= '' or brand=:brand) and (:model= '' or model=:model) and " +
            "(:fuel= '' or fuel=:fuel) and (:price=0 or price<=:price)",
            nativeQuery = true)
    List<Advirtesement> getFilterAd(@Param("type") String type,
                                    @Param("brand") String brand,
                                    @Param("model")String model,
                                    @Param("fuel") String fuel,
                                    @Param("price") float price);
}
