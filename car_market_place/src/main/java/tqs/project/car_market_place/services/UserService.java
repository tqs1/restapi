package tqs.project.car_market_place.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import tqs.project.car_market_place.exceptions.InvalidBodyParameters;
import tqs.project.car_market_place.exceptions.InvalidInput;
import tqs.project.car_market_place.dto.UsersDTO;
import tqs.project.car_market_place.entities.Users;
import tqs.project.car_market_place.exceptions.InvalidUser;
import tqs.project.car_market_place.repositories.UsersRepository;

import java.util.List;

@Service
public class UserService {

    @Autowired
    public UsersRepository utilizadorRepository;

    public List<String> getUsers() {
        return utilizadorRepository.getUsers();
    }

    public List<String> getEmails() {
        return utilizadorRepository.getEmails();
    }

    public String getPassword(String username) {
        String pass = utilizadorRepository.getPassword(username);
        if (pass == null){
            throw new InvalidUser();
        }
        return pass;

    }

    public Users addNewUser(UsersDTO utilizadorDTO) {
        try {
            Users user = new Users(utilizadorDTO);
            utilizadorRepository.save(user);
            return user;
        } catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        } catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        } catch (NullPointerException e) {
            throw new InvalidInput();
        }
    }

    public String getEmail(String username) {
        String email = utilizadorRepository.getEmail(username);
        if (email == null){
            throw new InvalidUser();
        }
        return email;
    }

    public String getContact(String username) {
        String contact = utilizadorRepository.getContact(username);
        if (contact == null){
            throw new InvalidUser();
        }
        return contact;
    }

    public UsersDTO changeUsername(String username, String newUsername) {
        try{
            Users user = utilizadorRepository.findByUsername(username);
            if(user==null){
                throw new InvalidUser();
            }
            user.setUsername(newUsername);
            Users updateUser = utilizadorRepository.save(user);

            return new UsersDTO(updateUser);
        }catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }
    }

    public UsersDTO changePassword(String username, String newPassword) {
        try{
            Users user = utilizadorRepository.findByUsername(username);
            if(user==null){
                throw new InvalidUser();
            }
            user.setPassword(newPassword);
            Users updateUser = utilizadorRepository.save(user);

            return new UsersDTO(updateUser);
        }catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }
    }

    public UsersDTO changeEmail(String username, String newEmail) {
        try{
            Users user = utilizadorRepository.findByUsername(username);
            if(user==null){
                throw new InvalidUser();
            }
            user.setEmail(newEmail);
            Users updateUser = utilizadorRepository.save(user);

            return new UsersDTO(updateUser);
        }catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }

    }

    public UsersDTO changePhoto(String username, String newPhoto) {
        try{
            Users user = utilizadorRepository.findByUsername(username);
            if(user==null){
                throw new InvalidUser();
            }
            user.setPhoto(newPhoto);
            Users updateUser = utilizadorRepository.save(user);

            return new UsersDTO(updateUser);
        }catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }
    }

    public UsersDTO changeContact(String username, String newContact) {
        try{
            Users user = utilizadorRepository.findByUsername(username);
            if(user==null){
                throw new InvalidUser();
            }
            user.setContact(newContact);
            Users updateUser = utilizadorRepository.save(user);

            return new UsersDTO(updateUser);
        }catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }

    }

}
