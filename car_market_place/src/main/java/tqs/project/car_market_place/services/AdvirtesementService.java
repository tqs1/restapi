package tqs.project.car_market_place.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import tqs.project.car_market_place.dto.*;
import tqs.project.car_market_place.entities.Advirtesement;
import tqs.project.car_market_place.entities.Users;
import tqs.project.car_market_place.exceptions.InvalidBodyParameters;
import tqs.project.car_market_place.exceptions.InvalidInput;
import tqs.project.car_market_place.repositories.AdvirtesementRepository;
import tqs.project.car_market_place.repositories.UsersRepository;

import java.util.*;

@Service
public class AdvirtesementService {
    @Autowired
    public AdvirtesementRepository advirtesementRepository;

    @Autowired
    public UsersRepository usersRepository;

    public List<AdvirtesementSearchDTO> getAll(String type) {
        List<Advirtesement> allCars = advirtesementRepository.getAll(type);
        return adToDTO(allCars);
    }

    public List<AdvirtesementSearchDTO> adToDTO(List<Advirtesement> ad){
        List<AdvirtesementSearchDTO> toReturn = new ArrayList<>();
        for(Advirtesement a : ad){
            Users user = a.getUserID();
            toReturn.add(new AdvirtesementSearchDTO(a, user));
        }
        return toReturn;
    }

    public AdvirtesementDTO getAdByID(Long id) {
        try{
            Advirtesement ad = advirtesementRepository.findByID(id);
            Users user = ad.getUserID();
            return new AdvirtesementDTO(ad, user);
        }catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        }
    }

    public Advirtesement addNewAd(NewAdvirtesementDTO newAdvirtesementDTO) {
        try {
            Users user = usersRepository.findByUsername(newAdvirtesementDTO.getUsername());
            return advirtesementRepository.save(new Advirtesement(newAdvirtesementDTO, user));
        } catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }catch (NullPointerException e) {
            throw new InvalidInput();
        }
    }

    public AdvirtesementDTO changeAd(long adID, UpdatedAdvirtesementDTO updatedAdvirtesementDTO) {
        try {
            Advirtesement advirtesement = advirtesementRepository.findByID(adID);
            advirtesement.setMonth(updatedAdvirtesementDTO.getMonth());
            advirtesement.setYear(updatedAdvirtesementDTO.getYear());
            advirtesement.setBrand(updatedAdvirtesementDTO.getBrand());
            advirtesement.setModel(updatedAdvirtesementDTO.getModel());
            advirtesement.setVersion(updatedAdvirtesementDTO.getVersion());
            advirtesement.setFuel(updatedAdvirtesementDTO.getFuel());
            advirtesement.setKms(updatedAdvirtesementDTO.getKms());
            advirtesement.setCylinder(updatedAdvirtesementDTO.getCylinder());
            advirtesement.setPower(updatedAdvirtesementDTO.getPower());
            advirtesement.setColor(updatedAdvirtesementDTO.getColor());
            advirtesement.setPrice(updatedAdvirtesementDTO.getPrice());
            advirtesement.setImage(updatedAdvirtesementDTO.getImage());
            advirtesement.setAnnotations(updatedAdvirtesementDTO.getAnnotation());
            advirtesement.setLocation(updatedAdvirtesementDTO.getLocation());

            Advirtesement updateAd = advirtesementRepository.save(advirtesement);
            Users user = updateAd.getUserID();
            return new AdvirtesementDTO(updateAd, user);
        } catch (HttpServerErrorException.InternalServerError e){
            throw new InvalidBodyParameters();
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }
    }

    public boolean setSold(long adID) {
        try {
            Advirtesement advirtesement = advirtesementRepository.findByID(adID);
            advirtesement.setState(false);
            advirtesementRepository.save(advirtesement);
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public int deleteAd(long adID) {
        try {
            return advirtesementRepository.delete(adID);
        } catch (NullPointerException e) {
            throw new InvalidInput();
        }
    }

    public List<AdvirtesementSearchDTO> getUserSells(String username) {
        long userID = usersRepository.findIdByUsername(username);
        List<Advirtesement> userSells = advirtesementRepository.getUserSells(userID);
        return adToDTO(userSells);
    }

    public List<AdvirtesementSearchDTO> getFilterAd(String type, String brand, String model, String fuel, float price) {
        List<Advirtesement> getFilterAd = advirtesementRepository.getFilterAd(type, brand, model, fuel, price);
        return adToDTO(getFilterAd);
    }
}
