package tqs.project.car_market_place.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.web.client.HttpClientErrorException;
import tqs.project.car_market_place.dto.BrandDTO;
import tqs.project.car_market_place.entities.Brand;
import tqs.project.car_market_place.exceptions.InvalidBrandID;
import tqs.project.car_market_place.repositories.BrandRepository;
import java.util.ArrayList;
import java.util.List;

@Service
public class BrandService {

    @Autowired
    public BrandRepository brandRepository;

    public List<BrandDTO> getCarBrands() {
        return brandToDTO(brandRepository.getCarBrands());
    }

    public List<BrandDTO> getMotoBrands() {
        return brandToDTO(brandRepository.getMotoBrands());
    }

    public List<BrandDTO> brandToDTO(List<Brand> brands){
        List<BrandDTO> toReturn = new ArrayList<>();
        for(Brand brand : brands){
            toReturn.add(new BrandDTO(brand));
        }
        return toReturn;
    }

    public List<String> getModelsByBrandID(Long id) {
        try{
            return brandRepository.getModelsByBrandID(id);
        }catch (HttpClientErrorException.NotFound e){
            throw new InvalidBrandID();

        }
    }
}
