package tqs.project.car_market_place;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarMarketPlaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarMarketPlaceApplication.class);
    }

}
