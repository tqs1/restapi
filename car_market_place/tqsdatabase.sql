--sudo -u postgres -i
--psql carmarketplace


--user-tqsproj
--password-carmarketplace
--database-carmarketplace

drop table if exists model;
drop table if exists brand;
drop table if exists advirtesement;
drop table if exists users;

--tables
Create table users(
userID serial not null,
username varchar(50) unique not null,
email varchar(50) unique not null,
name varchar(300) not null,
password text not null, 
photo varchar,
contact varchar(9),

primary key(userID)

);

Create table advirtesement(
adID serial not null,
userID serial not null,
type varchar(4) not null,
state BOOLEAN not NULL,
time timestamp not null,
month INTEGER not null,
year INTEGER not null,
brand VARCHAR(50) not null,
model VARCHAR(50) not null,
version VARCHAR(50),
fuel VARCHAR(20) not null,
kms INTEGER not null,
cylinder INTEGER not null,
power INTEGER not null,
color VARCHAR(25) not null,
price float not null,
image varchar,
annotations Varchar(500),
location varchar(30),

primary key(adID),
foreign key(userID) references users(userID)
);

create table brand(
    brandID serial not null,
    brand varchar(50) not null,
    type varchar(4) not null,

    primary key(brandID)
);

create table model(
    modelID serial not null,
    model varchar(50) not null,
    brandID serial not null,

    primary key(modelID),
    foreign key(brandID) references brand(brandID)
);
	

--INSERTS

insert into users(username, email, name, password, photo, contact) values('carolina', 'carolina@ua.pt', 'Carolina Pereira', md5('carolina'), null, 123456789);

insert into users(username, email, name, password, photo, contact) values('jose', 'jose@ua.pt', 'José Moreira', md5('jose'), null, 917894561);

insert into users(username, email, name, password, photo, contact) values('tiago', 'tiago@ua.pt', 'Tiago Teixeira', md5('tiago'), null, 919191919);


insert into advirtesement (userID, type, state, time, month, year, brand, model, version, fuel,  kms, cylinder, power, color, price, image, annotations, location)
values (1, 'Car', true, '2020-05-19', 01, 2019,'Honda', 'Civic', null, 'Diesel',  18000, 1600, 120, 'Black', 19500, null, 'great condition', 'Braga');

insert into advirtesement (userID, type, state, time,month, year, brand, model, version, fuel,  kms, cylinder, power, color, price, image, annotations, location)
values (2, 'Car', true, '2019-05-23', 06, 2002,'Seat', 'Ibiza', '1.9 TDI Sport', 'Diesel',  303500, 1896, 130, 'White', 5750, null, null, 'Aveiro');

insert into advirtesement (userID, type, state, time,month, year, brand, model, version, fuel,  kms, cylinder, power, color, price, image, annotations, location)
values (3, 'Car', true, '2019-05-24', 10, 2014,'Mercedes', 'Classe A', null, 'Diesel',  170545, 1500, 109, 'Grey', 14000, null, null, 'Porto');

insert into advirtesement (userID, type, state, time,month, year, brand, model, version, fuel,  kms, cylinder, power, color, price, image, annotations, location)
values (2, 'Car', true, '2019-05-24', 04, 2015, 'BMW', 'Serie 1', null, 'Petrol',  70000, 1500, 116, 'Black', 15500, null, null, 'Aveiro');

insert into advirtesement (userID, type, state, time,month, year, brand, model, version, fuel,  kms, cylinder, power, color, price, image, annotations, location)
values (3, 'Moto', true, '2020-05-24', 09, 2019,'Honda', 'CBR', null, 'Petrol',  2000, 649, 95, 'Black', 7750, null, null, 'Porto');

insert into advirtesement (userID, type, state, time,month, year, brand, model, version, fuel,  kms, cylinder, power, color, price, image, annotations, location)
values (2, 'Moto', true, '2020-05-26', 07, 2018,'Yamaha', 'YZ', '250 f', 'Petrol',  5000, 250, 40, 'Blue', 4500, null, null, 'Aveiro');


insert into brand(brand, type) values ('Honda', 'Car');
insert into brand(brand, type) values ('Honda', 'Moto');
insert into brand(brand, type) values ('Seat', 'Car');
insert into brand(brand, type) values ('Mercedes', 'Car');
insert into brand(brand, type) values ('BMW', 'Car');
insert into brand(brand, type) values ('Yamaha', 'Moto');


insert into model(model, brandID) values('Civic', 1);
insert into model(model, brandID) values('Jazz', 1);
insert into model(model, brandID) values('S2000', 1);
insert into model(model, brandID) values('CBR', 2);
insert into model(model, brandID) values('CB', 2);
insert into model(model, brandID) values('VFR', 2);
insert into model(model, brandID) values('Ibiza', 3);
insert into model(model, brandID) values('Leon', 3);
insert into model(model, brandID) values('Altea', 3);
insert into model(model, brandID) values('Classe A', 4);
insert into model(model, brandID) values('Classe B', 4);
insert into model(model, brandID) values('Classe C', 4);
insert into model(model, brandID) values('Serie 1', 5);
insert into model(model, brandID) values('Serie 2', 5);
insert into model(model, brandID) values('Serie 3', 5);
insert into model(model, brandID) values('R6', 6);
insert into model(model, brandID) values('Super Ténéré', 6);
insert into model(model, brandID) values('YZ', 6);





--Selects
Select * from users;
Select * from advirtesement;
Select * from brand;
Select * from model;


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO tqsproj;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO tqsproj;








